#! /bin/bash
set -e

echo "Starting creation and calculations" > metrics.log
date >> metrics.log
echo >> metrics.log

# Fetching base data from Supabase, calculating and pushing results to Supabase
echo "Updating math data on Supabase!"
echo dashboard.etl.create_math_qa_data_on_supabase
echo $(python -m dashboard.etl.create_math_qa_data_on_supabase && echo "Supabase math data created successfully!" || echo "Error while creating Supabase math data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Uploading missing math data to Supabase"
echo dashboard.etl.upload_missing_math_data
echo $(python -m dashboard.etl.upload_missing_math_data && echo "Supabase math data created successfully!" || echo "Error while uploading missing math data!") >> metrics.log
echo >> metrics.log
sleep 3


echo "Updating full message data on Supabase!"
echo dashboard.etl.create_full_message_data_on_supabase
echo $(python -m dashboard.etl.create_full_message_data_on_supabase && echo "Full message data created successfully!" || echo "Error while creating Supabase full message data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Updating student data on Supabase!"
echo dashboard.etl.create_student_data_on_supabase
echo $(python -m dashboard.etl.create_student_data_on_supabase && echo "Student data created successfully!" || echo "Error while creating Supabase student data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "Updating student metrics on Supabase!"
echo dashboard.metrics.calculate_user_features
echo $(python -m dashboard.metrics.calculate_user_features && echo "Student Metrics created successfully!" || echo "Error while creating Supabase student data!") >> metrics.log
echo >> metrics.log
sleep 3

echo "End of the calculations!" >> metrics.log
echo >> metrics.log

exit 0
