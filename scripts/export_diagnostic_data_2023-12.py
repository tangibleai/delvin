import pandas as pd
import json

from dashboard.utils import select_from_supabase
import os,django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

STAGING_DB_NAME = "supabase-db-test"

query = (
    f"""
    select
        *
    from
      message
    where
      not (message.request_object ? 'line_number')
      and created_at > '2023-12-12'
    """
)
query_result = select_from_supabase(db_name=STAGING_DB_NAME, query=query)

for row in query_result:
    for field in ["nlu_response", "request_object"]:
        if field in row:
            # Convert the string to a dictionary
            row[field] = json.loads(row[field])

message_records_df = pd.json_normalize(query_result)

tabular_cols_needed = ["id"]
nlu_cols_needed = [
    "nlu_response.data",
    "nlu_response.type",
    "nlu_response.confidence",
]
new_ro_col_names = [
    "contact_uuid",
    "message_id",
    "message_updated_at",
    "message_body",
    "question",
    "expected_answer",
]
ro_cols_needed = ["request_object." + str for str in new_ro_col_names]

# choosing only the cols that we need
all_cols = tabular_cols_needed + nlu_cols_needed + ro_cols_needed
df_final = message_records_df[all_cols]

ro_cols_renaming = {}
for old_name, new_name in zip(ro_cols_needed, new_ro_col_names):
    ro_cols_renaming[old_name] = new_name
df_final = df_final.rename(columns=ro_cols_renaming)
df_final = df_final.rename(
    columns={"message_body": "text", "message_id": "original_message_id"}
)

df_final["message_inserted_at"] = df_final["message_updated_at"].apply(pd.Timestamp)
df_final = df_final.drop(columns=["message_updated_at", "id"])

new_nlu_cols = [col.replace(".", "_") for col in nlu_cols_needed]
df_final.rename(columns=dict(zip(nlu_cols_needed, new_nlu_cols)), inplace=True)
df_final["answer_type"] = df_final['nlu_response_type']


df_final.to_csv('diagnostic_test_results-2023-12.csv')