import os

import django
import pandas as pd

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()


from dashboard.utils import run_custom_query


query = """
        SELECT 
          contact_uuid, 
          inserted_at,
          message_text,
          CASE
            WHEN prev_message_text LIKE '%How old are you?%' THEN 'age'
            WHEN prev_message_text LIKE '%whose phone are you currently using?%' then 'phone_type'
            WHEN prev_message_text LIKE '%Which exam are you preparing for?%' then 'exam'
            WHEN prev_message_text LIKE '%What math topics were you looking to practise%' then 'advanced_topics'
            else NULL
          END as question
        FROM
          (
            select
              id,
              message_text,
              contact_uuid, 
              inserted_at,
              lag(message_text, 1) over (
                partition by
                  contact_uuid
                order by
                  id
              ) as prev_message_text
            from
              bq_messages
            where
              inserted_at BETWEEN '2024-02-01' AND '2024-02-27'
            ) messages
        WHERE (prev_message_text LIKE '%How old are you?%') or (prev_message_text LIKE '%whose phone are you currently using?%') or (prev_message_text LIKE '%Which exam are you preparing for?%') or (prev_message_text LIKE '%What math topics were you looking to practise%')
        """

df = run_custom_query(query)
df_min_time = df.groupby(['contact_uuid' ]).agg({'inserted_at': 'min'})
df_agg = df.groupby(['contact_uuid', 'question'])['message_text'].agg('last').reset_index()

df_pivoted = df_agg.pivot(index=['contact_uuid'], columns='question', values='message_text')
df_pivoted['start_timestamp'] = df_min_time['inserted_at']
df_pivoted = df_pivoted.sort_values(by=['contact_uuid'])
df_pivoted["age"] = df_pivoted["age"].str.extract(r'([0-9]+)')

df_pivoted.to_csv('open_line_onboarding_survey_with_student_details_2024-02-01-2024-02-26.csv')