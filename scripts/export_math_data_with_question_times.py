import os
import time
import pandas as pd
import datetime

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()

from dashboard.utils import select_from_supabase
from streamlit_frontend.src.const import *
from streamlit_frontend.src.sql_utils import WhereClause

supabase_db_name = "supabase-db-rori"

fields = [
    FN_MSG_TEXT,
    FN_QUESTION_TEXT,
    FN_EXPECTED_ANS,
    FN_LEVEL,
    FN_LESSON,
    FN_SKILL,
    FN_TOPIC,
    FN_QUESTION_NUM,
    FN_LINE_NUM,
    "nlu_response_data",
    "nlu_response_type",
    "nlu_response_confidence",
    "supabase_id"
]


date_limit_list = [
    (datetime.date(2024,1,1), datetime.date(2024, 1, 30)),
]

full_df = None

for start_date, end_date in date_limit_list:
    where_clause = WhereClause()
    where_clause.add_datelimit_subclause(
        "inserted_at", from_date=start_date, to_date=end_date
    )

    print(
        f"Pulling data from supabase: {start_date.strftime('%Y-%m-%d')}-{end_date.strftime('%Y-%m-%d')}"
    )
    query = f"""
            select
                  {','.join(fields)},
                  math_messages.original_message_id as original_message_id,
                  messages_with_prev.inserted_at as message_inserted_at,
                  messages_with_prev.contact_uuid as contact_uuid,
                  messages_with_prev.prev_inserted_at as question_timestamp, 
                  answer_type
                from
                  (
                    select
                      original_message_id, 
                      answer_type,
                      {','.join(fields)}
                    from
                      math_question_answer
                  ) as math_messages
                  inner join (
                    select
                      contact_uuid,
                      original_message_id,
                      inserted_at,
                      LAG(inserted_at) OVER (
                        PARTITION BY
                          contact_uuid
                        ORDER BY
                          id
                      ) AS prev_inserted_at,
                      LAG(direction) OVER (
                        PARTITION BY
                          contact_uuid
                        ORDER BY
                          id
                      ) AS prev_direction
                    FROM
                      bq_messages_rising
                    {where_clause.get_clause()}
                  ) as messages_with_prev on math_messages.original_message_id = messages_with_prev.original_message_id
                where
                  messages_with_prev.prev_direction = 'outbound'
            """
    query_result = select_from_supabase(db_name=supabase_db_name, query=query)

    current_df = pd.DataFrame.from_records(query_result)

    if full_df is None:
        full_df = current_df.copy()
    else:
        full_df = pd.concat([full_df, current_df])

full_df.to_csv("math_question_answers_question_timestamps.2024-01-01-2024-01-30.rising_line.csv", index=False)

