"""Standalone script, can be run as `python scripts/fix_messages_data.py` or `python fix_messages_data.py`"""

import datetime
import os
import sys

from datetime import datetime

import django
import pandas as pd

path_to_file = "lessons_skills_topics.csv"
try:
    lesson_skills_topics_df = pd.read_csv(path_to_file)
except FileNotFoundError:
    path_to_file = "scripts/" + path_to_file
    lesson_skills_topics_df = pd.read_csv(path_to_file)

def setup():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")

    try:
        django.setup()
    except Exception:
        add_parent_dir_to_sys_path()
        django.setup()


def add_parent_dir_to_sys_path():
    """add current folder parent to"""
    current_file_path = __file__
    current_dir_path = os.path.dirname(os.path.abspath(current_file_path))
    parent_dir_path = os.path.dirname(current_dir_path)
    if parent_dir_path not in sys.path:
        sys.path.insert(0, parent_dir_path)
    return


setup()

from dashboard.models import (
    MathQuestionAnswerModel,
    MicrolessonModel,
    UnfoundMessageModel,
)
from core.constants import TEST_DATABASE_NAME


def save_unfound_message_to_db(message_record):
    original_message_id = message_record.original_message_id
    records_with_original_message_id = UnfoundMessageModel.objects.using(
        TEST_DATABASE_NAME
    ).filter(original_message_id=original_message_id)
    if not records_with_original_message_id:
        UnfoundMessageModel.objects.using(TEST_DATABASE_NAME).create(**message_record)
    return


def run():
    start_date = datetime(2024, 1, 31)
    end_date = datetime(2024, 2, 8)
    math_question_answer_records = MathQuestionAnswerModel.objects.using(
        TEST_DATABASE_NAME
    ).filter(message_inserted_at__gte=start_date, message_inserted_at__lt=end_date)

    process_math_question_answer_records(math_question_answer_records)
    return


def process_math_question_answer_records(records):
    i = 0
    for record in records:
        i +=1
        question = record.question
        expected_answer = record.expected_answer
        microlesson_record = (
            MicrolessonModel.objects.using(TEST_DATABASE_NAME)
            .filter(question=question, expected_answer=expected_answer)
            .first()
        )
        if not microlesson_record:
            save_wrong_message_record_in_db_if_not_exist(record)
            continue
        update_math_question_answer_record(record, microlesson_record)
        if i%1000 == 0:
            print(f"Processed {i} records")
    return


def save_wrong_message_record_in_db_if_not_exist(math_question_answer_record):
    original_message_id = math_question_answer_record.original_message_id
    if not UnfoundMessageModel.objects.filter(original_message_id=original_message_id):
        unfound_message_record = UnfoundMessageModel()
        fields = [field.name for field in MathQuestionAnswerModel._meta.fields]
        for field in fields:
            setattr(
                unfound_message_record,
                field,
                getattr(math_question_answer_record, field),
            )
        unfound_message_record.save(using=TEST_DATABASE_NAME)
    return


def update_math_question_answer_record(math_question_answer_record, microlesson_record):
    question_microlesson = microlesson_record.question_microlesson
    math_question_answer_record.question_micro_lesson = (
        microlesson_record.question_microlesson
    )
    math_question_answer_record.question_level = int(question_microlesson[1])

    df = lesson_skills_topics_df

    row = df.loc[df["Microlessons(Lessons)"] == question_microlesson]
    question_topic = row["Menu 2 (Topic)"].values[0]
    question_skill = row["Menu 3 (Skill)"].values[0]
    math_question_answer_record.question_skill = question_skill
    math_question_answer_record.question_topic = question_topic
    math_question_answer_record.save(using=TEST_DATABASE_NAME)
    return


def get_lessons_skills_topics_dataframe():
    path_to_file = "lessons_skills_topics.csv"
    try:
        df = pd.read_csv(path_to_file)
    except FileNotFoundError:
        path_to_file = "scripts/" + path_to_file
        df = pd.read_csv(path_to_file)
    return df


if __name__ == "__main__":
    run()
