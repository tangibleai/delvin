import os

import django
import pandas as pd

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")
django.setup()


from dashboard.utils import run_custom_query


query = """
        SELECT 
          contact_uuid, 
          inserted_at,
          ROW_NUMBER() over (PARTITION BY contact_uuid, prev_message_text ORDER BY inserted_at ) as ans_num,  
          message_text,
          prev_message_text
        FROM
          (
            select
              id,
              message_text,
              contact_uuid, 
              inserted_at,
              lag(message_text, 1) over (
                partition by
                  contact_uuid
                order by
                  id
              ) as prev_message_text
            from
              bq_messages
            where
              inserted_at BETWEEN '2023-12-01' AND '2024-01-07'
            ) messages
        WHERE (prev_message_text LIKE '%How old are you?%') or (prev_message_text LIKE '%whose phone are you currently using?%') or (prev_message_text LIKE '%Which exam are you preparing for?%') or (prev_message_text LIKE 'How do you think Rori could be improved?%')
        """

df = run_custom_query(query)
df_min_time = df.groupby(['contact_uuid', 'ans_num']).agg({'inserted_at': 'min'})

df_pivoted = df.pivot(index=['contact_uuid', 'ans_num'], columns='question', values='message_text')
df_pivoted['start_timestamp'] = df_min_time['inserted_at']
df_pivoted = df_pivoted.sort_values(by=['contact_uuid'])

query_stud_details = """
                    select
                      contact_uuid,
                      num_sessions,
                      num_active_days,
                      num_active_weeks,
                      num_lessons_visited,
                      num_lessons_completed,
                      average_accuracy_rate
                    from
                      student_metrics
                    where
                      inserted_at >= '2023-12-01' and inserted_at <= '2024-01-07'
                    """


df_stud_details = run_custom_query(query_stud_details)

df = pd.merge(df_pivoted, df_stud_details, on=['contact_uuid'], how="inner")

df.to_csv('open_line_survey_with_student_details_2023-12-01-2024-01-07.csv')