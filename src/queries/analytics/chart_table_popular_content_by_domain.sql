/*
CONTEXT:
- Creates a table that shows how many users accessed each content domain (without those that accessed none)
RESULT EXPECTATION
- Generates a table with two columns:
* domain_tag_name - the domain tag
* unique_users - number of unique users that accessed content tagged with domain_tag_name
*/
WITH card_with_tag AS
(
-- select the most recent version of the content tags
WITH cte AS
(
  SELECT
  *
  FROM
    (
    SELECT
     uuid
     ,title
     ,content
     ,language
     ,updated_at
     ,ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
    FROM
     `sample-turnio-bot-20201103.15550159753.cards`
    WHERE
     is_deleted IS false
    )
  WHERE
  rownumber = 1
)
-- join the content with domain content tags

SELECT
 cte.uuid AS card_uuid
 ,cte.content AS content
 ,cte.title AS title
 ,cte.rownumber AS rownumber
 ,domain_tags.tag_name AS domain_tag
FROM
  (
    cte
    LEFT JOIN
    (
      SELECT
        *
      FROM `sample-turnio-bot-20201103.15550159753.card_tag_map`
      WHERE tag_id IN
        (
        SELECT
        id
        FROM
        `sample-turnio-bot-20201103.15550159753.tangibleai_tags`
        WHERE
        tag_category = 'domain_tag'
        )
    ) AS card_tag_map ON cte.uuid = card_tag_map.card_uuid
    LEFT JOIN
    (
    SELECT
      *
    FROM `sample-turnio-bot-20201103.15550159753.tangibleai_tags`
    WHERE tag_category = 'domain_tag'
    ) AS domain_tags ON card_tag_map.tag_id = domain_tags.id
  )
)

-- create a ranking of content pieces
SELECT
 domain_tag
 ,title
 ,COUNT(DISTINCT chat_id) AS unique_users
FROM
  (
  SELECT
    ms.chat_id
    ,ct.*
  FROM `sample-turnio-bot-20201103.15550159753.messages` AS ms
  RIGHT JOIN card_with_tag AS ct ON ms.card_uuid = ct.card_uuid
  )
WHERE
  domain_tag = '<tag_name>'
GROUP BY
  domain_tag,title
ORDER BY unique_users DESC
