
select 
    a.question_number as question_number,   
    round((coalesce(b.correct,0)/a.answered)*100,2) as correct_asnwered_perc
from
    (select question_number, count(distinct chat_id) as answered from `sample-turnio-bot-20201103.15550159753.nndrc_messages_augmented` 
    where question_number is not null and button_tag is not null and chat_id is not null
    group by question_number)a
left outer join
    (select question_number, count(distinct chat_id) as correct from `sample-turnio-bot-20201103.15550159753.nndrc_messages_augmented` 
    where question_number is not null and button_tag = 'Correct Answer' and chat_id is not null
    group by question_number) b
on a.question_number = b.question_number
order by question_number