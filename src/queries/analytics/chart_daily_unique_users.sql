/* 
CONTEXT: 
- Generates the daily unique users chart
RESULT EXPECTATION
- returns 
  x - a range of dates, ascending order
  y - number of unique users for each date 
*/

SELECT 
    DATE(inserted_at) as x, 
    COUNT(DISTINCT chat_id) as y 
FROM 
    {{variables.tables.messages_augmented}} 
GROUP BY x 
ORDER BY x


/*---------- Transformation ----------- */
return data.map((row) => { 
   
  return {
    x: row.x.value,
    y: row.y,  
  };
});