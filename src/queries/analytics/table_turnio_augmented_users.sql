/* 
CONTEXT: 
- Adds additional information to Turn.io messages
RESULT EXPECTATION
- All the fields of `messages` table plus: 
  * message_text - the text of the message, no matter message type
  * prev_message_text - the text of the previous message in conversation with this user
*/
SELECT 
      chats_unique.chat_id as chat_id
      ,chats_unique.state as state
      ,contact_details_unique.contact_id as contact_id
      ,contact_details_unique.fields as fields
      /*
      --flattening the fields
        , case
              when fields LIKE '%"gender":"OPTION1"%' then 'option1'
              when fields LIKE '%"gender":"HOMME"%' then 'male'
              else 'unknown'
            end as gender,
        case
              when fields LIKE '%"age":"MOINSDE20ANS"%' then '<20'
              when fields LIKE '%"age":"DE2049ANS"%' then '20-49'
              when fields LIKE '%"age":"PLUSDE50ANS%' then '>50'
              else 'unknown'
            end as age 
         , REGEXP_EXTRACT(fields,'"field_name":"([^\"]+)\"' ) as field_name
        */
      ,chats_unique.chat_inserted_at as inserted_at
      ,chats_unique.chat_updated_at as updated_at
    FROM
      (SELECT
        id as chat_id
        ,contact_id
        ,state
        ,inserted_at as chat_inserted_at
        ,updated_at as chat_updated_at
      FROM
        (
        SELECT
        id 
        ,contact_id
        ,state
        ,inserted_at
        ,updated_at

        ,ROW_NUMBER() OVER (PARTITION BY id ORDER BY updated_at DESC) AS rownumber
        FROM
          {chats}
        )
      WHERE
      rownumber = 1) as chats_unique
    INNER JOIN
      (
        SELECT
            contact_id
            ,fields
            ,updated_at as contact_updated_at
          FROM
            (
            SELECT
            contact_id
            ,fields
            ,updated_at
            ,ROW_NUMBER() OVER (PARTITION BY contact_id ORDER BY updated_at DESC) AS rownumber
            FROM
              `{contact_details}
            )
          WHERE
          rownumber = 1
          ) as contact_details_unique
    ON chats_unique.contact_id = contact_details_unique.contact_id