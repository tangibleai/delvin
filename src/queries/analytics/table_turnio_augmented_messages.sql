/* 
CONTEXT: 
- Adds additional information to Turn.io messages
RESULT EXPECTATION
- All the fields of `messages` table plus: 
  * message_text - the text of the message, no matter message type
  * prev_message_text - the text of the previous message in conversation with this user
*/
SELECT
  *, 
  LAG(message_text,1) OVER (PARTITION BY chat_id ORDER BY inserted_at) as prev_message_text
FROM
(
SELECT 
 *, 
   case
    WHEN messages.content is not null then messages.content
    WHEN messages.content is null and messages.message_type in ('voice', 'image', 'video', 'ephemeral', 'contacts', 'document', 'audio', 'sticker') then  CONCAT('{', messages.message_type, '}')
    WHEN messages.content is null and messages.button_reply_title is not NULL then messages.button_reply_title
    WHEN messages.content is null and messages.list_reply_title is not NULL then messages.list_reply_title
    WHEN messages.content is null and messages.interactive is not null and messages.interactive!='null' then REGEXP_EXTRACT(messages.interactive,'"text":"([^\"]+)\"' )
  end as message_text
FROM {{messages_table}} as messages 
)