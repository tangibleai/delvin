WITH cte AS
(
  SELECT 
    *
  FROM
    (
    SELECT   
      uuid ,
      title ,
      content ,
      language ,
      updated_at ,
      ROW_NUMBER() OVER (PARTITION BY uuid ORDER BY updated_at DESC) AS rownumber
    FROM     
      `sample-turnio-bot-20201103.15550159753.cards`
    WHERE    
      is_deleted IS false )
  WHERE  rownumber=1 )


SELECT   
  domain,
  AVG(msg_num) AS average_domain_message
FROM     
  (
  SELECT     
    card_domain.domain_tag AS domain,
    msg.chat_id            AS user_id,
    COUNT(msg.id)          AS msg_num
  FROM       
    (
    SELECT    
      cte.uuid             AS card_uuid,
      domain_tags.tag_name AS domain_tag
    FROM      
      (
      cte
        LEFT JOIN
          (  
          SELECT 
            *
          FROM   
            `sample-turnio-bot-20201103.15550159753.card_tag_map`
          WHERE  
            tag_id IN
              (
              SELECT 
                id
              FROM   
                `sample-turnio-bot-20201103.15550159753.tangibleai_tags`
              WHERE  
                tag_category = 'domain_tag')) AS card_tag_map
        ON        
          cte.uuid=card_tag_map.card_uuid
            LEFT JOIN
              (
              SELECT 
                *
              FROM   
                `sample-turnio-bot-20201103.15550159753.tangibleai_tags`
              WHERE  
                tag_category = 'domain_tag')AS domain_tags
            ON        
              card_tag_map.tag_id = domain_tags.id )
    WHERE     
      domain_tags.tag_name IS NOT NULL) card_domain
        INNER JOIN 
          `sample-turnio-bot-20201103.15550159753.messages` AS msg
        ON         
          card_domain.card_uuid = msg.card_uuid
  GROUP BY   
    card_domain.domain_tag,
    msg.chat_id)
GROUP BY domain
