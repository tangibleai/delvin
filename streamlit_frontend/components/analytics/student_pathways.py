import streamlit as st

from streamlit_frontend.src.const import MATH_USER_SEGMENTS
from streamlit_frontend.charts.analytics_charts import (
    user_pathways_levels,
    user_pathways_lessons,
)


def display_component(date_clause):
    st.write("## Pathways Between Math Levels")
    _, col_prune, col_segment, col_compute, _ = st.columns([1, 2, 2, 2, 1])
    with col_prune:
        lev_link_prune_num = st.number_input(
            label="Prune links of less than", value=50, key="an_path_lev_prune"
        )
    with col_segment:
        segments = ["All"] + MATH_USER_SEGMENTS
        lev_segment_select = st.selectbox(
            "User Segment", segments, key="an_path_lev_seg"
        )
        if lev_segment_select == "All":
            lev_segment_select = None

    with col_compute:
        st.write("######")
        compute_pushed = st.button("Calculate", key="an_path_compute")

    if compute_pushed:
        fig = user_pathways_levels(
            date_clause,
            link_prune_val=int(lev_link_prune_num),
            segment=lev_segment_select,
        )
        st.plotly_chart(fig, use_container_width=True)
        compute_pushed = False

    st.write("## Pathways Between Lessons")
    (
        _,
        les_col_prune,
        les_col_segment,
        les_col_level,
        les_col_compute,
        _,
    ) = st.columns([1, 2, 2, 2, 2, 1])
    with les_col_prune:
        les_link_prune_num = st.number_input(
            label="Prune links of less than", value=50, key="an_path_les_prune"
        )
    with les_col_segment:
        segments = ["All"] + MATH_USER_SEGMENTS
        les_segment_select = st.selectbox(
            "User Segment", segments, key="an_path_les_seg"
        )
        if les_segment_select == "All":
            les_segment_select = None

    with les_col_level:
        level_options = range(1, 10)
        level = st.selectbox("Choose a level", level_options)

    with les_col_compute:
        st.write("######")
        les_compute_pushed = st.button("Calculate", key="an_path_les_compute")

    if les_compute_pushed:
        fig = user_pathways_lessons(
            date_clause,
            level,
            link_prune_val=int(les_link_prune_num),
            segment=les_segment_select,
        )
        st.plotly_chart(fig, use_container_width=True)
        les_compute_pushed = False
