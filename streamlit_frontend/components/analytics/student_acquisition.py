import streamlit as st

from streamlit_frontend.charts.analytics_charts import user_acquisition_funnel
from streamlit_frontend.components import double_chart
from streamlit_frontend.src.const import ALL_USER_SEGMENTS, NO_DATA_ERROR_MESSAGE


def display_component(date_clause, to_compare=False, date_clause_c=None):
    st.write("### User Acquisition Funnel")

    include_segments = st.multiselect(
        label="Choose User Segments",
        options=ALL_USER_SEGMENTS,
        default=ALL_USER_SEGMENTS,
    )

    if to_compare:
        double_chart.show_double_chart(
            date_clause,
            date_clause_c,
            user_acquisition_funnel,
            kwargs=dict(include_segments=include_segments),
        )
    else:
        fig = user_acquisition_funnel(date_clause, include_segments=include_segments)
        if fig is not None:
            st.plotly_chart(fig)
        else:
            st.write(NO_DATA_ERROR_MESSAGE)
