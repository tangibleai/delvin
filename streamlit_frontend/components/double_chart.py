import streamlit as st
from streamlit_frontend.src.const import NO_DATA_ERROR_MESSAGE, NO_DATA_EXPLANATION_DATE_RANGE


def show_double_chart(
    initial_date_clause, comparison_date_clause, chart_func, kwargs={}
):
    col_orig, col_comp = st.columns([1, 1])

    with col_orig:
        fig = chart_func(initial_date_clause, **kwargs)
        if fig is not None:
            st.plotly_chart(fig, use_container_width=True)
        else:
            st.write(NO_DATA_ERROR_MESSAGE)
            st.write(NO_DATA_EXPLANATION_DATE_RANGE)

    with col_comp:
        fig = chart_func(comparison_date_clause, is_comparison=True, **kwargs)
        if fig is not None:
            st.plotly_chart(fig, use_container_width=True)
        else:
            st.write(NO_DATA_ERROR_MESSAGE)
            st.write(NO_DATA_EXPLANATION_DATE_RANGE)

