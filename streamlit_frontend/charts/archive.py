# def onboarding_stage_by_time(date_clause=None, percentage=False):
#     where_string = ""
#     if date_clause:
#         where_string = date_clause.get_clause()
#
#     df = run_custom_query(
#         f"""
#         SELECT
#             DATE(inserted_at) AS date,
#             onboarding_stage,
#             count (distinct contact_uuid) as num_users
#         FROM
#             student_metrics
#         {where_string}
#         GROUP BY
#             DATE(inserted_at), onboarding_stage;
#         """
#     )
#
#     df["onboarding_stage"] = df["onboarding_stage"].fillna("Greeting")
#     print(df["onboarding_stage"].value_counts())
#
#     if percentage:
#         fig = px.area(
#             df,
#             x="date",
#             y="num_users",
#             color="onboarding_stage",
#             line_group="onboarding_stage",
#             groupnorm="percent",
#             category_orders={
#                 "onboarding_stage": [
#                     "Greeting",
#                     "Registration_AF",
#                     "Onboarding_AF",
#                     "Level",
#                     "Math Quiz",
#                 ]
#             },
#             hover_data=["num_users"],
#             labels={"num_users": "% of Users"},
#         )
#
#         fig.update_layout(
#             xaxis_title="Date",
#             yaxis_title="% of Users",
#             showlegend=True,
#             hovermode="x",
#         )
#
#     else:
#         fig = px.area(
#             df,
#             x="date",
#             y="num_users",
#             color="onboarding_stage",
#             line_group="onboarding_stage",
#             category_orders={
#                 "onboarding_stage": [
#                     "Greeting",
#                     "Registration_AF",
#                     "Onboarding_AF",
#                     "Level",
#                     "Math Quiz",
#                 ]
#             },
#             hover_data=["num_users"],
#             labels={"num_users": "Number of Users"},
#         )
#
#         fig.update_layout(
#             xaxis_title="Date",
#             yaxis_title="Number of Users",
#             showlegend=True,
#             hovermode="x",
#         )
#
#     return fig


# def user_segment_by_time(percentage=False):
#     df = run_custom_query(
#         """
#         SELECT
#             DATE(inserted_at) AS date,
#             user_segment,
#             count (distinct contact_uuid) as num_users
#         FROM
#             student_metrics
#         GROUP BY
#             DATE(inserted_at), user_segment;
#         """
#     )
#
#     if percentage:
#         fig = px.area(
#             df,
#             x="date",
#             y="num_users",
#             color="user_segment",
#             line_group="user_segment",
#             groupnorm="percent",
#             category_orders={"user_segment": ALL_USER_SEGMENTS},
#             hover_data=["num_users"],
#             labels={"num_users": "% of Users"},
#         )
#
#         fig.update_layout(
#             xaxis_title="Date",
#             yaxis_title="% of Users",
#             showlegend=True,
#             hovermode="x",
#         )
#
#     else:
#         fig = px.area(
#             df,
#             x="date",
#             y="num_users",
#             color="user_segment",
#             line_group="user_segment",
#             category_orders={"user_segment": ALL_USER_SEGMENTS},
#             hover_data=["num_users"],
#             labels={"num_users": "Number of Users"},
#         )
#
#         fig.update_layout(
#             xaxis_title="Date",
#             yaxis_title="Number of Users",
#             showlegend=True,
#             hovermode="x",
#         )
#
#     return fig

# def student_activity_chart(where_clause):
#     def discrete_colorscale(bvals, colors):
#         """
#         bvals - list of values bounding intervals/ranges of interest
#         colors - list of rgb or hex colorcodes for values in [bvals[k], bvals[k+1]],0<=k < len(bvals)-1
#         returns the plotly  discrete colorscale
#         """
#         if len(bvals) != len(colors) + 1:
#             raise ValueError("len(boundary values) should be equal to  len(colors)+1")
#         bvals = sorted(bvals)
#         nvals = [
#             (v - bvals[0]) / (bvals[-1] - bvals[0]) for v in bvals
#         ]  # normalized values
#
#         dcolorscale = []  # discrete colorscale
#         for k in range(len(colors)):
#             dcolorscale.extend([[nvals[k], colors[k]], [nvals[k + 1], colors[k]]])
#         return dcolorscale
#
#     query = """
#             SELECT
#                 contact_uuid,
#                 message_inserted_at,
#                 question_micro_lesson,
#                 question_number,
#                 CASE
#                     WHEN question_number='0' THEN 0.5
#                     WHEN answer_type='next' or answer_type='menu' THEN 0.0
#                     WHEN answer_type='failure' or answer_type='success' or answer_type='hint' THEN 0.25
#                     ELSE 0.75
#                 END as event_code,
#                 ROW_NUMBER() OVER (PARTITION by contact_uuid ORDER BY message_inserted_at) as rank
#             FROM
#                 (
#                 SELECT
#                 )
#             """
#     df = run_custom_query(query)
#     df["unique_question"] = df["question_micro_lesson"] + "-" - df["question_number"]
#     pivot_df = df.pivot(index="contact_uuid", columns="rank", values="event_code")
#
#     pivot_df["non_NaN_count"] = pivot_df.count(axis=1)
#     df_to_plot = pivot_df[pivot_df["non_NaN_count"] >= 100]
#     columns_to_plot = pivot_df.columns[:100]
#     df_to_plot = df_to_plot[columns_to_plot]
#     df_to_plot["percent_new"] = (
#         df_to_plot.apply(lambda row: (row == 0.5).sum(), axis=1) / 100
#     )
#     df_to_plot = df_to_plot.sort_values(by="percent_new")
#
#     bvalues = [0.0, 0.25, 0.5, 0.75, 1.0]
#     colors = ["rgb(255,0,0)", "rgb(0,255,0)", "rgb(0,0,255)", "rgb(200,200,200)"]
#
#     colorscale = discrete_colorscale(bvalues, colors)
#     bvals = np.array(bvalues)
#     tickvals = [
#         np.mean(bvals[k : k + 2]) for k in range(len(bvals) - 1)
#     ]  # position with respect to bvals where ticktext is displayed
#     ticktext = ["next/menu", "new lesson", "question answer", "other"]
#
#     fig = go.Figure(
#         data=go.Heatmap(
#             z=df_to_plot.values,
#             x=columns_to_plot,
#             y=df_to_plot.index,
#             colorscale=colorscale,
#             colorbar=dict(thickness=25, tickvals=tickvals, ticktext=ticktext),
#         )
#     )


# def user_pathways_activities(date_clause, link_prune_val=0):
#     """
#     Unfinished function - pulls both non-math actitivities (users activating a stack) and
#     math activities (complete/partial lessons) in sankey format. Has bugs in the sankey chart
#     """
#     non_math_activities = get_non_math_activities(date_clause)
#     math_activities = get_math_activities(date_clause)
#     all_activities = pd.concat([non_math_activities, math_activities])
#     all_activities = all_activities.sort_values(by=["user_id", "time_event"])
#     all_activities = all_activities.reset_index(0, drop=True)
#     st.write(
#         all_activities[all_activities["time_install"] > all_activities["time_event"]]
#     )
#     fig = sankey_chart(all_activities, max_rank=10, link_prune_val=link_prune_val)
#     return fig




