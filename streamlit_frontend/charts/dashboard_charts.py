import plotly.express as px
import pandas as pd

from dashboard.utils import run_custom_query
from streamlit_frontend.src.sql_utils import WhereClause


def num_active_users(date_clause):
    query = f"""
            SELECT 
              count(distinct contact_uuid) as result
            from 
              math_question_answer
            {date_clause.get_clause()} 
            """
    df = run_custom_query(query)
    result = df["result"].iloc[0]
    return result

def num_lessons_completed(date_clause):
    where_clause = WhereClause(date_clause)
    where_clause.add_str_subclause("question_number", "10")

    query = f"""
                 SELECT 
                  count(*) as result
                from 
                  (
                  select
                    contact_uuid,
                    question_micro_lesson, 
                    MAX(message_inserted_at) as inserted_at
                  from 
                    math_question_answer
                  {where_clause.get_clause()}
                  group by 
                    contact_uuid, question_micro_lesson
                  ) as completed_lessons
                """
    df = run_custom_query(query)
    result = df["result"].iloc[0]
    return result


def weekly_completed_lessons(date_clause):
    where_clause = WhereClause(date_clause)
    where_clause.add_str_subclause("question_number", "10")

    query = f"""
            select
              week_start, 
              line_number,
              count (*) as num_completions
            from
            (
              select
                contact_uuid,
                question_micro_lesson, 
                line_number,
                MAX(message_inserted_at) as inserted_at,
                date_trunc('week', message_inserted_at)::date as week_start
              FROM 
                math_question_answer
            {where_clause.get_clause()}
              GROUP BY 
                contact_uuid, question_micro_lesson,date_trunc('week', message_inserted_at)::date, line_number
            ) as completions_with_weeks
            group by week_start, line_number
            order by week_start
            """

    df = run_custom_query(query)

    # Create a stacked area chart
    fig = px.area(df, x='week_start', y='num_completions', color='line_number',
                  labels={'num_completions': 'Lessons Completed', 'line_number': 'Line Number'})

    return fig