import streamlit as st
from datetime import datetime

from streamlit_frontend.src.etl import (
    get_student_conversation_by_id,
    get_student_conversation_from_all_lines,
    search_convos_for_text,
)
from streamlit_frontend.src.const import DOMAIN

PAGE_SIZE = 20
MAX_SEARCH_RESULTS = 10


def init_state(stud_msgs, reset_message_id=False):
    if st.session_state["message_id"]:
        message_id = st.session_state["message_id"]
    else:
        message_id = None

    if "stud_msgs" not in st.session_state or reset_message_id:
        st.session_state["stud_msgs"] = stud_msgs

    if stud_msgs is None:
        return

    if message_id:
        row_number = (
            stud_msgs[stud_msgs["original_message_id"] == message_id].index[0]
            if not stud_msgs[stud_msgs["original_message_id"] == message_id].empty
            else None
        )
        init_from_msg = max(row_number - 1, 0)
    else:
        init_from_msg = max(len(stud_msgs) - PAGE_SIZE, 0)


    if "curr_from_msg" not in st.session_state or reset_message_id:
        st.session_state["curr_from_msg"] = init_from_msg

    if "max_msg" not in st.session_state or reset_message_id:
        st.session_state["max_msg"] = len(stud_msgs)

    if "curr_to_msg" not in st.session_state or reset_message_id:
        st.session_state["curr_to_msg"] = min(
            st.session_state["curr_from_msg"] + PAGE_SIZE, st.session_state["max_msg"]
        )

def on_slider_move():
    st.session_state["curr_from_msg"] = st.session_state["convo_slider"]
    st.session_state["curr_to_msg"] = min(
        st.session_state["curr_from_msg"] + PAGE_SIZE, st.session_state["max_msg"]
    )
    st.session_state["message_id"] = None


def on_go_to_convo_click(contact_uuid, message_id):
    st.session_state["contact_uuid"] = contact_uuid
    st.session_state["message_id"] = message_id

    stud_msgs = get_student_conversation_from_all_lines(st.session_state["contact_uuid"])
    init_state(stud_msgs, True)


# def load_earlier_messages():
#     st.session_state["curr_from_msg"] = max(st.session_state["curr_from_msg"] - PAGE_SIZE, 0)
#
# def load_later_messages():
#     st.session_state["curr_to_msg"] = min(st.session_state["curr_to_msg"] + PAGE_SIZE,
#                                           st.session_state["max_msg"])


def load_messages(student_messages, from_message, to_message):
    curr_messages = student_messages.iloc[from_message:to_message]

    for i, m in curr_messages.iterrows():
        col_time, col_message = st.columns([1, 5])

        with col_time:
            st.caption(
                f":gray[{datetime.strftime(m['inserted_at'], '%Y-%m-%d %H:%M:%S')}]"
            )

        with col_message:
            if m["direction"] == "inbound":
                with st.chat_message("user"):
                    st.write(m["message_text"])
            else:
                with st.chat_message(
                    "assistant",
                    avatar="https://images.squarespace-cdn.com/content/v1/6268eb8de6c2576f5d8f5a9a/ba250914-0a7d-49a8-b7b7-22d9892e6625/rori-500px.png",
                ):
                    st.write(m["message_text"])


def load_page(contact_uuid=None, message_id=None):
    if "contact_uuid" not in st.session_state:
        st.session_state["contact_uuid"] = contact_uuid

    if "message_id" not in st.session_state:
        st.session_state["message_id"] = message_id

    with st.form("Student_id"):
        col_input, col_button = st.columns([2, 1])
        default_value = (
            st.session_state["contact_uuid"]
            if st.session_state["contact_uuid"]
            else "356c40b6-ae76-4574-a36c-4d57c05ccbdd"
        )
        with col_input:
            single_contact_uuid = st.text_input(
                "Contact UUID",
                value=default_value,
                key="conversations_contact_uuid_field",
            )

        with col_button:
            st.write("###")
            submit = st.form_submit_button("Display Student")

        if submit:
            st.session_state["contact_uuid"] = single_contact_uuid
            stud_msgs = get_student_conversation_from_all_lines(st.session_state["contact_uuid"])
            init_state(stud_msgs, True)

    with st.expander("Search Conversations"):
        with st.form("Search Criteria"):
            col_input, col_userorbot, col_start_date, col_end_date = st.columns(
                [6, 2, 2, 2]
            )

            with col_input:
                search_text = st.text_input(
                    "Look for", key="conversations_look_for_text"
                )

            with col_userorbot:
                user_or_bot = st.selectbox("In messages from", ["user"])

            with col_start_date:
                start_date = st.date_input(
                    "From",
                    value=datetime(2023, 9, 1),
                    min_value=datetime(2023, 5, 16),
                    key="conversations_from_date",
                )

            with col_end_date:
                end_date = st.date_input(
                    "To", min_value=datetime(2023, 5, 16), key="conversations_to_date"
                )

            search_submit = st.form_submit_button("Search")

        if search_submit:
            search_result = search_convos_for_text(
                search_text, user_or_bot, start_date, end_date
            )

            if not len(search_result):
                st.write("No conversations found")

            else:
                for i, row in search_result.iterrows():
                    if i > MAX_SEARCH_RESULTS:
                        break

                    st.caption(
                        datetime.strftime(row["message_inserted_at"], "%Y-%m-%d")
                    )

                    col_convo, col_button = st.columns([5, 1])

                    with col_convo:
                        bot_message = st.chat_message("assistant")
                        bot_message.write(f"{row['question'][0:100]}")
                        user_message = st.chat_message("user")
                        user_message.write(f"{row['text']}")

                    with col_button:
                        st.button(
                            "Go to Conversation",
                            key=(row["original_message_id"] + "convo"),
                            on_click=on_go_to_convo_click,
                            args=(row["contact_uuid"], row["original_message_id"]),
                        )

                    st.divider()

    if st.session_state["contact_uuid"]:
        stud_msgs = get_student_conversation_from_all_lines(st.session_state["contact_uuid"])

        if stud_msgs is None:
            st.write("## No student found")
            return

        init_state(stud_msgs)


        col_label, col_link = st.columns([1, 4])

        with col_label:
            st.write("**Share this conversation:**")

        with col_link:
            c_id = st.session_state["contact_uuid"]
            m_id = (
                st.session_state["message_id"]
                if st.session_state["message_id"]
                else stud_msgs.iloc[st.session_state["curr_from_msg"] + 1][
                    "original_message_id"
                ]
            )
            convo_url = (
                f"" f"{DOMAIN}/?menu_index=3&contact_uuid={c_id}&message_id={m_id}"
            )
            st.code(convo_url, language="http")

        st.slider(
            "Browse conversation",
            min_value=0,
            max_value=len(stud_msgs),
            value=st.session_state["curr_from_msg"],
            on_change=on_slider_move,
            key="convo_slider",
        )

        _, col_button_left, col_title, col_button_right, _ = st.columns([1, 1, 3, 1, 1])

        if st.session_state["curr_from_msg"] > 0:
            with st.expander("10 earlier messages"):
                load_messages(
                    stud_msgs,
                    max(st.session_state["curr_from_msg"] - 10, 0),
                    st.session_state["curr_from_msg"],
                )

        load_messages(
            stud_msgs,
            st.session_state["curr_from_msg"],
            st.session_state["curr_to_msg"],
        )

        if st.session_state["curr_to_msg"] < st.session_state["max_msg"]:
            with st.expander("10 later messages"):
                load_messages(
                    stud_msgs,
                    min(
                        st.session_state["curr_to_msg"] + 1, st.session_state["max_msg"]
                    ),
                    min(
                        st.session_state["curr_to_msg"] + 11,
                        st.session_state["max_msg"],
                    ),
                )
