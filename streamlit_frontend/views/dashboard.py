import streamlit as st
from datetime import datetime, date

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.charts.dashboard_charts import num_active_users, num_lessons_completed, weekly_completed_lessons
from streamlit_frontend.src.const import RORI_OPEN_LINE, RORI_RISING_LINE, FN_LINE_NUM

def load_page():
    col_title,_, col_start_date, col_dash, col_end_date, _, col_line_selector\
        = st.columns([7,2,3,1,3,1,3])

    with col_title:
        st.write("## Dashboard")

    with col_start_date:
        start_date = st.date_input(
            "From", value=datetime(2023, 5, 16), min_value=datetime(2023, 5, 16)
        )

    with col_end_date:
        end_date = st.date_input("To", min_value=datetime(2023, 5, 16))

    with col_line_selector:
        select_line = st.selectbox(
            "Rori line/s", ["Both Lines", "Open Line Only", "Rising Line Only"]
        )

    math_date_clause = WhereClause()
    math_date_clause.add_datelimit_subclause("message_inserted_at", start_date, end_date)

    if select_line == "Rising Line Only":
        math_date_clause.add_int_subclause(field=FN_LINE_NUM, value=RORI_RISING_LINE)
    elif select_line == "Open Line Only":
        math_date_clause.add_int_subclause(
            field=FN_LINE_NUM,
            value=RORI_OPEN_LINE,
        )
    else:
        numbers = (RORI_RISING_LINE, RORI_OPEN_LINE)
        math_date_clause.add_int_subclause(
            field=FN_LINE_NUM,
            value=numbers,
            operator=' IN '
        )

    col_active, col_lessons_completed,_  = st.columns([3,3,3])


    with col_active:
        active_math_students = num_active_users(math_date_clause)
        st.metric("Active Math Students", active_math_students)

    with col_lessons_completed:
        lessons_completed = num_lessons_completed(math_date_clause)
        st.metric("Lessons Completed", lessons_completed)

    fig = weekly_completed_lessons(math_date_clause)
    st.plotly_chart(fig)

