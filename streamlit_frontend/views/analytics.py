import streamlit as st
from datetime import datetime, date
import pandas as pd

from streamlit_frontend.charts.analytics_charts import (
    user_pathways_levels,
    user_pathways_lessons,
)

from streamlit_frontend.src.etl import get_answers_for_question, export_df
from streamlit_frontend.components.analytics import (
    student_progress,
    student_acquisition,
    student_pathways,
)
from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.const import (
    POPULAR_BOT_QUESTIONS,
)


def load_page():
    st.write("## User Analysis (Open line only)")
    _, col_stud_joined, col_start_date, col_end_date, _ = st.columns([1, 2, 1, 1, 1])

    with col_stud_joined:
        st.write("View Students that Joined")
        st.write("#")

    with col_start_date:
        start_date = st.date_input(
            "From",
            value=datetime(2023, 5, 16),
            min_value=datetime(2023, 1, 13),
            key="analytics_from",
        )

    if start_date < date(2023, 5, 16):
        st.warning(
            "Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing",
            icon="⚠️",
        )

    with col_end_date:
        end_date = st.date_input(
            "To", min_value=datetime(2023, 5, 16), key="analytics_to"
        )

    # choose comparison date range
    _, col_stud_joined_c, col_start_date_c, col_end_date_c, _ = st.columns(
        [1, 2, 1, 1, 1]
    )

    with col_stud_joined_c:
        to_compare = st.checkbox("Compare to date range:", key="analytics_to_compare")

    if to_compare:
        with col_start_date_c:
            start_date_c = st.date_input(
                "From",
                value=datetime(2023, 8, 1),
                min_value=datetime(2023, 1, 13),
                key="analytics_from_comp",
            )

        if start_date_c < date(2023, 5, 16):
            st.warning(
                "Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing",
                icon="⚠️",
            )

        with col_end_date_c:
            end_date_c = st.date_input(
                "To",
                value=datetime(2023, 8, 31),
                min_value=datetime(2023, 5, 16),
                key="to_comp",
            )

    if start_date < date(2023, 5, 16):
        st.warning(
            "Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023, Math-related charts might be inaccurate or missing",
            icon="⚠️",
        )

    date_clause = WhereClause()
    date_clause.add_datelimit_subclause("inserted_at", start_date, end_date)

    if to_compare:
        date_clause_c = WhereClause()
        date_clause_c.add_datelimit_subclause("inserted_at", start_date_c, end_date_c)

    tab_acquisition, tab_progress, tab_pathways, tab_responses = st.tabs(
        ["User Acquisition", "User Progress", "User Pathways", "User Responses"]
    )

    with tab_acquisition:
        if to_compare:
            student_acquisition.display_component(
                date_clause, to_compare, date_clause_c
            )

        else:
            student_acquisition.display_component(date_clause)

    with tab_progress:
        if to_compare:
            student_progress.display_component(date_clause, to_compare, date_clause_c)

        else:
            student_progress.display_component(date_clause)

    with tab_pathways:
        student_pathways.display_component(date_clause)

    with tab_responses:
        st.write("### Popular bot questions")
        popular_q = pd.DataFrame(
            POPULAR_BOT_QUESTIONS, columns=["Question Name", "Search for text"]
        )
        st.table(popular_q)

        with st.form("Find user responses"):
            question_text = st.text_input(
                "Bot Question Text", key="analytics_user_responses_question_text"
            )
            submit = st.form_submit_button("Find user responses")

        if submit:
            question_answers = get_answers_for_question(question_text, date_clause)
            csv = export_df(question_answers)
            col_responses, col_download = st.columns([4, 1])
            with col_responses:
                st.write("User responses to bot question:")
                st.dataframe(question_answers)

            with col_download:
                st.download_button(
                    label="Download data as CSV",
                    data=csv,
                    file_name="question_responses.csv",
                    mime="text/csv",
                )
