import pandas as pd
import streamlit as st
import copy

from dashboard.utils import select_from_supabase, bulk_load_from_supabase

from streamlit_frontend.src.sql_utils import WhereClause
from streamlit_frontend.src.const import *

from streamlit_frontend.charts.qe_overview_charts import (
    COL_TOTAL_ANSWERS,
    COL_WRONG_ANSWERS,
    COL_CORRECT_ANSWERS,
    COL_HINT_REQUESTS,
    COL_NUM_USERS,
)


@st.cache_data(show_spinner="Loading data from Supabase...")
def load_batch_of_sb_math_data(batch_size, offset):
    field_list = QUESTION_EXPLORER_FIELDS
    query = f"SELECT {','.join(field_list)} from math_question_answer LIMIT {batch_size} OFFSET {offset};"
    test = bulk_load_from_supabase(SB_DB_NAME, query)
    return test


def load_sb_math_data(date):
    sb_math_data = []
    batch_size = 1000
    offset = 0
    are_records_remaining = True

    while are_records_remaining:
        test = load_batch_of_sb_math_data(batch_size, offset)
        sb_math_data.extend(test)
        offset += batch_size

        if len(test) < batch_size:
            are_records_remaining = False
            break

    return pd.DataFrame.from_records(sb_math_data)


def get_student_records_by_id(fields, contact_uuid):
    query = f"""SELECT {",".join(fields)}
                          FROM math_question_answer
                          WHERE contact_uuid='{contact_uuid}' 
                          ORDER BY message_inserted_at
                       """

    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    df = pd.DataFrame.from_records(query_result)
    if not len(df):
        return pd.DataFrame()

    df["message_inserted_at"] = pd.to_datetime(df["message_inserted_at"])
    df = df.sort_values(by="message_inserted_at")
    return df


def get_student_conversation_by_id(contact_uuid, line_number=RORI_OPEN_LINE):

    table_name = 'bq_messages' if line_number == RORI_OPEN_LINE else 'bq_messages_rising'
    query = f"""SELECT 
                    id,
                    original_message_id,
                    message_text, 
                    direction, 
                    inserted_at
                FROM 
                    {table_name}
                WHERE
                    contact_uuid='{contact_uuid}'
                ORDER BY id
                """

    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    df = pd.DataFrame.from_records(query_result)
    if not len(df):
        return None

    df["inserted_at"] = pd.to_datetime(df["inserted_at"])
    return df

def get_student_conversation_from_all_lines(contact_uuid):
    student_convo_open = get_student_conversation_by_id(contact_uuid, RORI_OPEN_LINE)

    if student_convo_open is not None:
        return student_convo_open
    else:
        student_convo_rising = get_student_conversation_by_id(contact_uuid, RORI_RISING_LINE)

        if student_convo_rising is not None:
            return student_convo_rising
        else:
            return None


@st.cache_data
def export_df(df):
    return df.to_csv().encode("utf-8")


def multiple_user_search(variable, operator, threshold=0, stud_segment=None, limit=10):
    where_clause = WhereClause()
    where_clause.add_int_subclause(field=variable, value=threshold, operator=operator)
    where_clause.add_int_subclause(
        field="num_lessons_visited", value="null", operator=" is not "
    )

    if stud_segment:
        where_clause.add_str_subclause(field="user_segment", value=stud_segment)

    query = f"""SELECT contact_uuid, inserted_at FROM student_metrics 
                {where_clause.get_clause()}
                ORDER BY inserted_at DESC
                LIMIT {limit};
             """
    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(query_result)


def get_math_event_data(field_name, date_clause, segment=None, level=None):
    where_clause = copy.deepcopy(date_clause)

    if segment:
        segment_subclause = f"user_segment='{segment}'"
        where_clause.add_subclause(segment_subclause)

    level_clause = f"WHERE question_level={level}" if level else ""

    query = f"""
    select
      levels.contact_uuid as user_id,
      contacts.inserted_at as time_install,
      levels.{field_name} as event_name,
      levels.time_level as time_event
    from
      (
        select
          contact_uuid,
          {field_name},
          min(message_inserted_at) as time_level
        from
          math_question_answer
        {level_clause}
        group by
          contact_uuid,
          {field_name}
      ) as levels
      left join (
      SELECT contact_uuid, inserted_at
      FROM student_metrics
      {where_clause.get_clause()}
       )as contacts 
       on levels.contact_uuid = contacts.contact_uuid
    """

    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    df = pd.DataFrame.from_records(query_result)

    if field_name == "question_level":
        df["event_name"] = "Level " + df["event_name"].apply(str)

    df = df.dropna()
    return df


def get_max_question_table():
    query = """
            SELECT 
                question_microlesson as question_micro_lesson,
                MAX(question_number)-1 as last_question
            FROM
                microlesson
            GROUP BY 
                question_microlesson
            """
    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    last_question_df = pd.DataFrame.from_records(query_result)
    return last_question_df


def get_math_data_with_clause(fields, where_clause):
    query = f"""
             SELECT {",".join(fields)} 
             FROM math_question_answer
             {where_clause.get_clause()}  
             ORDER BY message_inserted_at DESC 
             """
    query_result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    df = pd.DataFrame.from_records(query_result)
    return df


def get_question_versions(question_clause):
    query = f"""
        SELECT 
            question,
            expected_answer, 
            min(message_inserted_at) as first_occurence
        from 
            math_question_answer
        {question_clause.get_clause()}
        GROUP BY question, expected_answer
        ORDER BY min(message_inserted_at) DESC
    """
    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(result)


def get_question_metrics_data(where_clause):
    no_zero_clause = WhereClause(where_clause)
    no_zero_clause.add_str_subclause(field="question_number", operator="!=", value="0")
    query = f"""
            select
              question_micro_lesson,
              question_number,
              concat('Q', question_number, ' - ', question_micro_lesson) as question_and_lesson,
              min(question_skill) as question_skill, 
              min(question_topic) as question_topic,
              count(*) as {COL_TOTAL_ANSWERS},
              count(distinct contact_uuid) as num_users,
              sum(
                case
                  when answer_type = 'success' then 1
                  else 0
                end
              ) as {COL_CORRECT_ANSWERS},
              sum(
                case
                  when answer_type = 'failure' then 1
                  else 0
                end
              ) as failure_answers,
              sum(
                case
                  when hint_shown = 'true' or nlu_response_data='hint' then 1
                  else 0
                end
              ) as num_hints
            from
              math_question_answer
            {where_clause.get_clause()}
            group by
              question_and_lesson,
              question_micro_lesson,
              question_number;
             """
    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(result)


def get_lesson_metrics(where_clause):
    no_zero_clause = WhereClause(where_clause)
    no_zero_clause.add_str_subclause(field="question_number", operator="!=", value="0")

    query = f"""
            select
              question_micro_lesson,
              min(question_skill) as question_skill, 
              min(question_topic) as question_topic,
              count(*) as {COL_TOTAL_ANSWERS},
              count(distinct contact_uuid) as {COL_NUM_USERS},
              sum(
                case
                  when answer_type = 'success' then 1
                  else 0
                end
              ) as {COL_CORRECT_ANSWERS},
              sum(
                case
                  when answer_type = 'failure' then 1
                  else 0
                end
              ) as {COL_WRONG_ANSWERS},
              sum(
                case
                  when hint_shown = 'true' or nlu_response_data='hint' then 1
                  else 0
                end
              ) as {COL_HINT_REQUESTS}
            from
              math_question_answer
            {no_zero_clause.get_clause()}
            group by
              question_micro_lesson;
            """
    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(result)


def get_data_for_qe_selectors(where_clause):
    query = f"""
                SELECT
                    {FN_LEVEL},
                    {FN_LESSON}, 
                    min({FN_TOPIC}) as {FN_TOPIC}, 
                    min({FN_SKILL}) as {FN_SKILL}, 
                    {FN_QUESTION_NUM}
                FROM 
                    math_question_answer
                {where_clause.get_clause()}
                GROUP BY
                    {FN_LEVEL}, 
                    {FN_LESSON}, 
                    {FN_QUESTION_NUM}
            """
    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(result)


def get_single_student_metric(metric, date_clause):
    query = f"""
             SELECT 
                 contact_uuid, 
                 {metric} 
             FROM 
                 student_metrics
             {date_clause.get_clause()};
            """
    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    df = pd.DataFrame.from_records(result)
    if not len(df):
        return None
    
    df = df.fillna(0)
    return df


def search_convos_for_text(
    search_text, user_or_bot, start_date, end_date, data="Math Data"
):
    fields = [
        "contact_uuid",
        "original_message_id",
        "text",
        "question",
        "message_inserted_at",
    ]

    where_clause = WhereClause()
    where_clause.add_datelimit_subclause("message_inserted_at", start_date, end_date)

    search_text = "%" + search_text + "%"
    where_clause.add_str_subclause(field="text", operator=" LIKE ", value=search_text)

    df = get_math_data_with_clause(fields, where_clause)
    df["message_inserted_at"] = pd.to_datetime(df["message_inserted_at"])
    return df


def get_answers_for_question(question, date_clause):
    query = f"""
            SELECT 
              message_text, 
              count (distinct id) as num_answers    
            FROM
              (
                select
                  id,
                  message_text,
                  lag(message_text, 1) over (
                    partition by
                      contact_uuid
                    order by
                      id
                  ) as prev_message_text
                from
                  bq_messages
                {date_clause.get_clause()}
                ) messages
            WHERE prev_message_text LIKE '%{question}%'
            GROUP BY message_text
            ORDER BY count (distinct id) desc
            """

    result = select_from_supabase(db_name=SB_DB_NAME, query=query)
    return pd.DataFrame.from_records(result)



