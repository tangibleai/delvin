menu_icons = {
    "Dashboard": "house-door-fill",
    "User Analysis": "bar-chart-fill",
    "Students": "mortarboard-fill",
    "Math Questions": "infinity",
    "Conversations": "chat-dots-fill",
    "Natural Language": "translation",
}
