import json
import pandas as pd
from pytz import UTC

from dashboard.utils import select_from_supabase

PRODUCTION_DB_NAME = "supabase-db-rori"

FULL_INFO_ROLLOUT_SUPABASE_ID = 647988
V3_CONTENT_ROLLOUT_SUPABASE_ID = 1249797
V3_ENDPOINT_ROLLOUT_SUPABASE_ID = 1276333


def get_max_id(table_name, field):
    """
    Finds the maximum supabase_id in math_question_answer
    """
    print("Finding max_id")

    # Query string to select data from Supabase with Django connection
    query = f"select max({field}) as max_id from {table_name};"
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)
    max_id = query_result[0]["max_id"] or 0

    print(f"max id: {max_id}")
    return max_id


def get_message_records_batch(batch_size):
    # Fetches Supabase data starting from the max_id_ parameter
    max_id_ = get_max_id(table_name="math_question_answer", field="supabase_id")

    query = (
        f"select * from message where id>{max_id_} "
        f"order by id "
        f"limit {batch_size};"
    )
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)

    for row in query_result:
        for field in ["nlu_response", "request_object"]:
            if field in row:
                # Convert the string to a dictionary
                row[field] = json.loads(row[field])

    df = pd.json_normalize(query_result)
    return df


def get_missing_message_records(batch_size):
    query = f"""
            SELECT t1.id, t1.nlu_response, t1.request_object
              FROM
                message as t1
                LEFT JOIN math_question_answer t2 ON t2.original_message_id = t1.request_object ->> 'message_id'
              WHERE t2.original_message_id IS NULL and t1.request_object::varchar LIKE '%question_number%'
            LIMIT {batch_size}
            """
    query_result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)

    for row in query_result:
        for field in ["nlu_response", "request_object"]:
            if field in row:
                # Convert the string to a dictionary
                row[field] = json.loads(row[field])

    df = pd.json_normalize(query_result)
    return df


def calc_new_question_number(row):
    """
    Calculates the new question number based on content change on 2023.08.31
    """
    res = 0
    qn = int(row["question_number"])

    if row["supabase_id"] > V3_CONTENT_ROLLOUT_SUPABASE_ID:
        if qn < 10:
            res = qn - 4
        else:
            res = qn - 6
    else:
        res = qn

    if res < 0:
        res = qn

    if not isinstance(res, int):
        pass

    return res


def calc_ans_type_v2(row):
    if row["nlu_response_type"] == "intents" or row["nlu_response_type"] == "keyword":
        if row["expected_answer"] == "Yes" and row["nlu_response_data"] == "yes":
            return "success"
        if (
            row["nlu_response_data"] == "answer"
            or row["nlu_response_data"] == "numerical_expression"
        ):
            return "failure"
        else:
            return row["nlu_response_data"].lower()
    elif str(row["nlu_response_data"]) == str(row["expected_answer"]) or str(
        row["text"]
    ) == str(row["expected_answer"]):
        return "success"
    else:
        return "failure"


def calc_ans_type_v3(row):
    if row["nlu_response_type"] == "intent" or row["nlu_response_type"] == "keyword":
        if ["nlu_response_data"] == "math_answer":
            return "failure"
        else:
            return row["nlu_response_data"].lower()

    elif row["nlu_response_type"] == "out_of_scope":
        return "out_of_scope"

    elif str(row["nlu_response_data"]) == str(row["expected_answer"]) or str(
        row["text"]
    ) == str(row["expected_answer"]):
        return "success"

    else:
        return "failure"


def calc_answer_type(row):
    if int(row["supabase_id"]) > V3_ENDPOINT_ROLLOUT_SUPABASE_ID:
        return calc_ans_type_v3(row)
    else:
        return calc_ans_type_v2(row)


def select_mqa_cols(message_records_df):
    """
    Selects all necessary columns from the original df and renames them
    """
    tabular_cols_needed = ["id"]
    nlu_cols_needed = [
        "nlu_response.data",
        "nlu_response.type",
        "nlu_response.confidence",
    ]
    new_ro_col_names = [
        "contact_uuid",
        "message_id",
        "message_updated_at",
        "message_body",
        "question",
        "question_level",
        "question_skill",
        "question_topic",
        "question_number",
        "expected_answer",
        "question_micro_lesson",
        "line_name",
        "line_number",
    ]
    ro_cols_needed = ["request_object." + str for str in new_ro_col_names]

    # choosing only the cols that we need
    all_cols = tabular_cols_needed + nlu_cols_needed + ro_cols_needed
    df_final = message_records_df[all_cols]

    hint_col_name = "request_object.hint_shown"
    if hint_col_name in message_records_df.columns:
        df_final["hint_shown"] = message_records_df[hint_col_name]

    ro_cols_renaming = {}
    for old_name, new_name in zip(ro_cols_needed, new_ro_col_names):
        ro_cols_renaming[old_name] = new_name
    df_final = df_final.rename(columns=ro_cols_renaming)
    df_final = df_final.rename(
        columns={"message_body": "text", "message_id": "original_message_id"}
    )
    df_final = df_final.rename(columns={"id": "supabase_id"})
    df_final["message_inserted_at"] = df_final["message_updated_at"].apply(pd.Timestamp)
    df_final = df_final.drop(columns=["message_updated_at"])

    new_nlu_cols = [col.replace(".", "_") for col in nlu_cols_needed]
    df_final.rename(columns=dict(zip(nlu_cols_needed, new_nlu_cols)), inplace=True)

    return df_final


def remove_junk_mqa_records(df_final):
    df_final.dropna(
        subset=[
            "question_level",
            "question_number",
            "question_micro_lesson",
            "expected_answer",
        ],
        inplace=True,
    )
    index_empty = df_final[
        (df_final["question_level"] == "")
        | (df_final["question_number"] == "")
        | (df_final["question_micro_lesson"] == "")
        | (df_final["expected_answer"] == "")
        | (df_final["line_name"] == "runninghill-244")
        | (df_final["line_name"] == "Rori Staging")
        | (df_final["line_name"] == "TEST")
        | (df_final["question_micro_lesson"] == "Microlessons(Lessons)")
    ].index
    df_clean = df_final.drop(index_empty)

    if len(index_empty):
        print(f"Omitted {len(index_empty)} empty rows")

    df_clean = df_clean.reset_index(drop=True)

    return df_clean


def etl_mqa_records(message_records_df):
    df_final = select_mqa_cols(message_records_df)
    # data validation - droping rows continuing NAs and empty strings
    df_final = remove_junk_mqa_records(df_final)

    if not len(df_final):
        return df_final

    # calculate the type of the answer
    df_final["new_question_number"] = df_final.apply(
        lambda row: calc_new_question_number(row), axis=1
    )
    df_final["question_number"] = df_final["new_question_number"]
    df_final = df_final.drop(columns=["new_question_number"])
    df_final["answer_type"] = df_final.apply(calc_answer_type, axis=1)

    df_final.sort_values(by="message_inserted_at", inplace=True)
    return df_final


if __name__ == "__main__":
    query = """
            select
  m.contact_uuid as contact_uuid,
  m.message_text as student_input,
  m.next_message_text as rori_generative_response
from
  (SELECT original_message_id from math_question_answer where nlu_response_type = 'out_of_scope') a
  inner join
  (SELECT original_message_id,
        contact_uuid,
        message_text,
        lead(message_text) over (
          partition by
            contact_uuid
          order by
            id
        ) as next_message_text
    FROM bq_messages
    WHERE id>569171224
  ) m
  on m.original_message_id = a.original_message_id
          """

    result = select_from_supabase(db_name=PRODUCTION_DB_NAME, query=query)
    df = pd.DataFrame.from_records(result)
    df.to_csv("generative_responses.csv")


COUNTRY_SQL_SUBQUERY = """
                        CASE
                            WHEN SUBSTRING(urn,1,5) = '+1684' THEN 'American Samoa'
                            WHEN SUBSTRING(urn,1,5) = '+1264' THEN 'Anguilla'
                            WHEN SUBSTRING(urn,1,5) = '+1284' THEN 'British Virgin Islands'
                            WHEN SUBSTRING(urn,1,5) = '+1345' THEN 'Cayman Islands'
                            WHEN SUBSTRING(urn,1,5) = '+1671' THEN 'Guam'
                            WHEN SUBSTRING(urn,1,5) = '+1664' THEN 'Montserrat'
                            WHEN SUBSTRING(urn,1,5) = '+1670' THEN 'Northern Mariana Islands'
                            WHEN SUBSTRING(urn,1,5) = '+1599' THEN 'Saint Martin'
                            WHEN SUBSTRING(urn,1,5) = '+1809' THEN 'Dominican Republic'
                            WHEN SUBSTRING(urn,1,5) = '+1829' THEN 'Dominican Republic'
                            WHEN SUBSTRING(urn,1,5) = '+1670' THEN 'Jamaica'
                            WHEN SUBSTRING(urn,1,5) = '+1876' THEN 'Puerto Rico'
                            WHEN SUBSTRING(urn,1,5) = '+1787' THEN 'Puerto Rico'
                            WHEN SUBSTRING(urn,1,4) = '+355' THEN 'Albania'
                            WHEN SUBSTRING(urn,1,4) = '+213' THEN 'Algeria'
                            WHEN SUBSTRING(urn,1,4) = '+376' THEN 'Andorra'
                            WHEN SUBSTRING(urn,1,4) = '+244' THEN 'Angola'
                            WHEN SUBSTRING(urn,1,4) = '+672' THEN 'Antarctica, Norfolk Island'
                            WHEN SUBSTRING(urn,1,4) = '+374' THEN 'Armenia'
                            WHEN SUBSTRING(urn,1,4) = '+297' THEN 'Aruba'
                            WHEN SUBSTRING(urn,1,4) = '+994' THEN 'Azerbaijan'
                            WHEN SUBSTRING(urn,1,4) = '+973' THEN 'Bahrain'
                            WHEN SUBSTRING(urn,1,4) = '+880' THEN 'Bangladesh'
                            WHEN SUBSTRING(urn,1,4) = '+375' THEN 'Belarus'
                            WHEN SUBSTRING(urn,1,4) = '+501' THEN 'Belize'
                            WHEN SUBSTRING(urn,1,4) = '+229' THEN 'Benin'
                            WHEN SUBSTRING(urn,1,4) = '+975' THEN 'Bhutan'
                            WHEN SUBSTRING(urn,1,4) = '+591' THEN 'Bolivia'
                            WHEN SUBSTRING(urn,1,4) = '+387' THEN 'Bosnia and Herzegovina'
                            WHEN SUBSTRING(urn,1,4) = '+267' THEN 'Botswana'
                            WHEN SUBSTRING(urn,1,4) = '+673' THEN 'Brunei'
                            WHEN SUBSTRING(urn,1,4) = '+359' THEN 'Bulgaria'
                            WHEN SUBSTRING(urn,1,4) = '+226' THEN 'Burkina Faso'
                            WHEN SUBSTRING(urn,1,4) = '+257' THEN 'Burundi'
                            WHEN SUBSTRING(urn,1,4) = '+855' THEN 'Cambodia'
                            WHEN SUBSTRING(urn,1,4) = '+237' THEN 'Cameroon'
                            WHEN SUBSTRING(urn,1,4) = '+238' THEN 'Cape Verde'
                            WHEN SUBSTRING(urn,1,4) = '+236' THEN 'Central African Republic'
                            WHEN SUBSTRING(urn,1,4) = '+235' THEN 'Chad'
                            WHEN SUBSTRING(urn,1,4) = '+269' THEN 'Comoros'
                            WHEN SUBSTRING(urn,1,4) = '+682' THEN 'Cook Islands'
                            WHEN SUBSTRING(urn,1,4) = '+506' THEN 'Costa Rica'
                            WHEN SUBSTRING(urn,1,4) = '+385' THEN 'Croatia'
                            WHEN SUBSTRING(urn,1,4) = '+599' THEN 'Curacao'
                            WHEN SUBSTRING(urn,1,4) = '+357' THEN 'Cyprus'
                            WHEN SUBSTRING(urn,1,4) = '+420' THEN 'Czech Republic'
                            WHEN SUBSTRING(urn,1,4) = '+243' THEN 'Democratic Republic of Congo'
                            WHEN SUBSTRING(urn,1,4) = '+253' THEN 'Djibouti'
                            WHEN SUBSTRING(urn,1,4) = '+670' THEN 'East Timor'
                            WHEN SUBSTRING(urn,1,4) = '+593' THEN 'Ecuador'
                            WHEN SUBSTRING(urn,1,4) = '+503' THEN 'El Salvador'
                            WHEN SUBSTRING(urn,1,4) = '+240' THEN 'Equatorial Guinea'
                            WHEN SUBSTRING(urn,1,4) = '+291' THEN 'Eritrea'
                            WHEN SUBSTRING(urn,1,4) = '+372' THEN 'Estonia'
                            WHEN SUBSTRING(urn,1,4) = '+251' THEN 'Ethiopia'
                            WHEN SUBSTRING(urn,1,4) = '+500' THEN 'Falkland Islands'
                            WHEN SUBSTRING(urn,1,4) = '+298' THEN 'Faroe Islands'
                            WHEN SUBSTRING(urn,1,4) = '+679' THEN 'Fiji'
                            WHEN SUBSTRING(urn,1,4) = '+358' THEN 'Finland'
                            WHEN SUBSTRING(urn,1,4) = '+689' THEN 'French Polynesia'
                            WHEN SUBSTRING(urn,1,4) = '+241' THEN 'Gabon'
                            WHEN SUBSTRING(urn,1,4) = '+220' THEN 'Gambia'
                            WHEN SUBSTRING(urn,1,4) = '+995' THEN 'Georgia'
                            WHEN SUBSTRING(urn,1,4) = '+233' THEN 'Ghana'
                            WHEN SUBSTRING(urn,1,4) = '+350' THEN 'Gibraltar'
                            WHEN SUBSTRING(urn,1,4) = '+299' THEN 'Greenland'
                            WHEN SUBSTRING(urn,1,4) = '+590' THEN 'Guadeloupe, Saint Barthélemy'
                            WHEN SUBSTRING(urn,1,4) = '+502' THEN 'Guatemala'
                            WHEN SUBSTRING(urn,1,4) = '+224' THEN 'Guinea'
                            WHEN SUBSTRING(urn,1,4) = '+245' THEN 'Guinea-Bissau'
                            WHEN SUBSTRING(urn,1,4) = '+592' THEN 'Guyana'
                            WHEN SUBSTRING(urn,1,4) = '+509' THEN 'Haiti'
                            WHEN SUBSTRING(urn,1,4) = '+504' THEN 'Honduras'
                            WHEN SUBSTRING(urn,1,4) = '+852' THEN 'Hong Kong'
                            WHEN SUBSTRING(urn,1,4) = '+354' THEN 'Iceland'
                            WHEN SUBSTRING(urn,1,4) = '+964' THEN 'Iraq'
                            WHEN SUBSTRING(urn,1,4) = '+353' THEN 'Ireland'
                            WHEN SUBSTRING(urn,1,4) = '+972' THEN 'Israel'
                            WHEN SUBSTRING(urn,1,4) = '+225' THEN 'Ivory Coast'
                            WHEN SUBSTRING(urn,1,4) = '+962' THEN 'Jordan'
                            WHEN SUBSTRING(urn,1,4) = '+254' THEN 'Kenya'
                            WHEN SUBSTRING(urn,1,4) = '+686' THEN 'Kiribati'
                            WHEN SUBSTRING(urn,1,4) = '+381' THEN 'Kosovo, Serbia'
                            WHEN SUBSTRING(urn,1,4) = '+965' THEN 'Kuwait'
                            WHEN SUBSTRING(urn,1,4) = '+996' THEN 'Kyrgyzstan'
                            WHEN SUBSTRING(urn,1,4) = '+856' THEN 'Laos'
                            WHEN SUBSTRING(urn,1,4) = '+371' THEN 'Latvia'
                            WHEN SUBSTRING(urn,1,4) = '+961' THEN 'Lebanon'
                            WHEN SUBSTRING(urn,1,4) = '+266' THEN 'Lesotho'
                            WHEN SUBSTRING(urn,1,4) = '+231' THEN 'Liberia'
                            WHEN SUBSTRING(urn,1,4) = '+218' THEN 'Libya'
                            WHEN SUBSTRING(urn,1,4) = '+423' THEN 'Liechtenstein'
                            WHEN SUBSTRING(urn,1,4) = '+370' THEN 'Lithuania'
                            WHEN SUBSTRING(urn,1,4) = '+352' THEN 'Luxembourg'
                            WHEN SUBSTRING(urn,1,4) = '+853' THEN 'Macau'
                            WHEN SUBSTRING(urn,1,4) = '+389' THEN 'Macedonia'
                            WHEN SUBSTRING(urn,1,4) = '+261' THEN 'Madagascar'
                            WHEN SUBSTRING(urn,1,4) = '+265' THEN 'Malawi'
                            WHEN SUBSTRING(urn,1,4) = '+960' THEN 'Maldives'
                            WHEN SUBSTRING(urn,1,4) = '+223' THEN 'Mali'
                            WHEN SUBSTRING(urn,1,4) = '+356' THEN 'Malta'
                            WHEN SUBSTRING(urn,1,4) = '+692' THEN 'Marshall Islands'
                            WHEN SUBSTRING(urn,1,4) = '+222' THEN 'Mauritania'
                            WHEN SUBSTRING(urn,1,4) = '+230' THEN 'Mauritius'
                            WHEN SUBSTRING(urn,1,4) = '+691' THEN 'Micronesia'
                            WHEN SUBSTRING(urn,1,4) = '+373' THEN 'Moldova'
                            WHEN SUBSTRING(urn,1,4) = '+377' THEN 'Monaco'
                            WHEN SUBSTRING(urn,1,4) = '+976' THEN 'Mongolia'
                            WHEN SUBSTRING(urn,1,4) = '+382' THEN 'Montenegro'
                            WHEN SUBSTRING(urn,1,4) = '+212' THEN 'Morocco, Western Sahara'
                            WHEN SUBSTRING(urn,1,4) = '+258' THEN 'Mozambique'
                            WHEN SUBSTRING(urn,1,4) = '+264' THEN 'Namibia'
                            WHEN SUBSTRING(urn,1,4) = '+674' THEN 'Nauru'
                            WHEN SUBSTRING(urn,1,4) = '+977' THEN 'Nepal'
                            WHEN SUBSTRING(urn,1,4) = '+687' THEN 'New Caledonia'
                            WHEN SUBSTRING(urn,1,4) = '+505' THEN 'Nicaragua'
                            WHEN SUBSTRING(urn,1,4) = '+227' THEN 'Niger'
                            WHEN SUBSTRING(urn,1,4) = '+234' THEN 'Nigeria'
                            WHEN SUBSTRING(urn,1,4) = '+683' THEN 'Niue'
                            WHEN SUBSTRING(urn,1,4) = '+850' THEN 'North Korea'
                            WHEN SUBSTRING(urn,1,4) = '+968' THEN 'Oman'
                            WHEN SUBSTRING(urn,1,4) = '+680' THEN 'Palau'
                            WHEN SUBSTRING(urn,1,4) = '+507' THEN 'Panama'
                            WHEN SUBSTRING(urn,1,4) = '+675' THEN 'Papua New Guinea'
                            WHEN SUBSTRING(urn,1,4) = '+595' THEN 'Paraguay'
                            WHEN SUBSTRING(urn,1,4) = '+870' THEN 'Pitcairn Islands'
                            WHEN SUBSTRING(urn,1,4) = '+351' THEN 'Portugal'
                            WHEN SUBSTRING(urn,1,4) = '+974' THEN 'Qatar'
                            WHEN SUBSTRING(urn,1,4) = '+242' THEN 'Republic of the Congo'
                            WHEN SUBSTRING(urn,1,4) = '+262' THEN 'Reunion'
                            WHEN SUBSTRING(urn,1,4) = '+250' THEN 'Rwanda'
                            WHEN SUBSTRING(urn,1,4) = '+290' THEN 'Saint Helena'
                            WHEN SUBSTRING(urn,1,4) = '+508' THEN 'Saint Pierre and Miquelon'
                            WHEN SUBSTRING(urn,1,4) = '+685' THEN 'Samoa'
                            WHEN SUBSTRING(urn,1,4) = '+378' THEN 'San Marino'
                            WHEN SUBSTRING(urn,1,4) = '+239' THEN 'Sao Tome and Principe'
                            WHEN SUBSTRING(urn,1,4) = '+966' THEN 'Saudi Arabia'
                            WHEN SUBSTRING(urn,1,4) = '+221' THEN 'Senegal'
                            WHEN SUBSTRING(urn,1,4) = '+381' THEN 'Serbia'
                            WHEN SUBSTRING(urn,1,4) = '+248' THEN 'Seychelles'
                            WHEN SUBSTRING(urn,1,4) = '+232' THEN 'Sierra Leone'
                            WHEN SUBSTRING(urn,1,4) = '+421' THEN 'Slovakia'
                            WHEN SUBSTRING(urn,1,4) = '+386' THEN 'Slovenia'
                            WHEN SUBSTRING(urn,1,4) = '+677' THEN 'Solomon Islands'
                            WHEN SUBSTRING(urn,1,4) = '+252' THEN 'Somalia'
                            WHEN SUBSTRING(urn,1,4) = '+211' THEN 'South Sudan'
                            WHEN SUBSTRING(urn,1,4) = '+249' THEN 'Sudan'
                            WHEN SUBSTRING(urn,1,4) = '+597' THEN 'Suriname'
                            WHEN SUBSTRING(urn,1,4) = '+268' THEN 'Swaziland'
                            WHEN SUBSTRING(urn,1,4) = '+963' THEN 'Syria'
                            WHEN SUBSTRING(urn,1,4) = '+886' THEN 'Taiwan'
                            WHEN SUBSTRING(urn,1,4) = '+992' THEN 'Tajikistan'
                            WHEN SUBSTRING(urn,1,4) = '+255' THEN 'Tanzania'
                            WHEN SUBSTRING(urn,1,4) = '+228' THEN 'Togo'
                            WHEN SUBSTRING(urn,1,4) = '+690' THEN 'Tokelau'
                            WHEN SUBSTRING(urn,1,4) = '+216' THEN 'Tunisia'
                            WHEN SUBSTRING(urn,1,4) = '+993' THEN 'Turkmenistan'
                            WHEN SUBSTRING(urn,1,4) = '+688' THEN 'Tuvalu'
                            WHEN SUBSTRING(urn,1,4) = '+256' THEN 'Uganda'
                            WHEN SUBSTRING(urn,1,4) = '+380' THEN 'Ukraine'
                            WHEN SUBSTRING(urn,1,4) = '+971' THEN 'United Arab Emirates'
                            WHEN SUBSTRING(urn,1,4) = '+598' THEN 'Uruguay'
                            WHEN SUBSTRING(urn,1,4) = '+998' THEN 'Uzbekistan'
                            WHEN SUBSTRING(urn,1,4) = '+678' THEN 'Vanuatu'
                            WHEN SUBSTRING(urn,1,4) = '+967' THEN 'Yemen'
                            WHEN SUBSTRING(urn,1,4) = '+260' THEN 'Zambia'
                            WHEN SUBSTRING(urn,1,4) = '+263' THEN 'Zimbabwe'
                            WHEN SUBSTRING(urn,1,3) = '+93' THEN 'Afghanistan'
                            WHEN SUBSTRING(urn,1,3) = '+54' THEN 'Argentina'
                            WHEN SUBSTRING(urn,1,3) = '+61' THEN 'Australia'
                            WHEN SUBSTRING(urn,1,3) = '+43' THEN 'Austria'
                            WHEN SUBSTRING(urn,1,3) = '+32' THEN 'Belgium'
                            WHEN SUBSTRING(urn,1,3) = '+55' THEN 'Brazil'
                            WHEN SUBSTRING(urn,1,3) = '+56' THEN 'Chile'
                            WHEN SUBSTRING(urn,1,3) = '+86' THEN 'China'
                            WHEN SUBSTRING(urn,1,3) = '+57' THEN 'Colombia'
                            WHEN SUBSTRING(urn,1,3) = '+53' THEN 'Cuba'
                            WHEN SUBSTRING(urn,1,3) = '+45' THEN 'Denmark'
                            WHEN SUBSTRING(urn,1,3) = '+20' THEN 'Egypt'
                            WHEN SUBSTRING(urn,1,3) = '+33' THEN 'France'
                            WHEN SUBSTRING(urn,1,3) = '+49' THEN 'Germany'
                            WHEN SUBSTRING(urn,1,3) = '+30' THEN 'Greece'
                            WHEN SUBSTRING(urn,1,3) = '+36' THEN 'Hungary'
                            WHEN SUBSTRING(urn,1,3) = '+91' THEN 'India'
                            WHEN SUBSTRING(urn,1,3) = '+62' THEN 'Indonesia'
                            WHEN SUBSTRING(urn,1,3) = '+98' THEN 'Iran'
                            WHEN SUBSTRING(urn,1,3) = '+44' THEN 'United Kingdom, Isle of Man'
                            WHEN SUBSTRING(urn,1,3) = '+39' THEN 'Italy, Vatican'
                            WHEN SUBSTRING(urn,1,3) = '+81' THEN 'Japan'
                            WHEN SUBSTRING(urn,1,3) = '+60' THEN 'Malaysia'
                            WHEN SUBSTRING(urn,1,3) = '+52' THEN 'Mexico'
                            WHEN SUBSTRING(urn,1,3) = '+95' THEN 'Myanmar [Burma]'
                            WHEN SUBSTRING(urn,1,3) = '+31' THEN 'Netherlands'
                            WHEN SUBSTRING(urn,1,3) = '+64' THEN 'New Zealand'
                            WHEN SUBSTRING(urn,1,3) = '+47' THEN 'Norway'
                            WHEN SUBSTRING(urn,1,3) = '+92' THEN 'Pakistan'
                            WHEN SUBSTRING(urn,1,3) = '+51' THEN 'Peru'
                            WHEN SUBSTRING(urn,1,3) = '+63' THEN 'Philippines'
                            WHEN SUBSTRING(urn,1,3) = '+48' THEN 'Poland'
                            WHEN SUBSTRING(urn,1,3) = '+40' THEN 'Romania'
                            WHEN SUBSTRING(urn,1,3) = '+65' THEN 'Singapore'
                            WHEN SUBSTRING(urn,1,3) = '+27' THEN 'South Africa'
                            WHEN SUBSTRING(urn,1,3) = '+82' THEN 'South Korea'
                            WHEN SUBSTRING(urn,1,3) = '+34' THEN 'Spain'
                            WHEN SUBSTRING(urn,1,3) = '+94' THEN 'Sri Lanka'
                            WHEN SUBSTRING(urn,1,3) = '+46' THEN 'Sweden'
                            WHEN SUBSTRING(urn,1,3) = '+41' THEN 'Switzerland'
                            WHEN SUBSTRING(urn,1,3) = '+66' THEN 'Thailand'
                            WHEN SUBSTRING(urn,1,3) = '+90' THEN 'Turkey'
                            WHEN SUBSTRING(urn,1,3) = '+58' THEN 'Venezuela'
                            WHEN SUBSTRING(urn,1,3) = '+84' THEN 'Vietnam'
                            WHEN SUBSTRING(urn,1,2) = '+1' THEN 'United States and Caribbean'
                            WHEN SUBSTRING(urn,1,2) = '+7' THEN 'Kazakhstan, Russia'
                            ELSE 'unknown'
                        END as country
                       """