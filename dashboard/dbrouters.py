"""Define the models which should be used by a Supabase Database"""

from dashboard.models import (
    MessagesModel,
    BQMessageModel,
    BQMessageModelRising,
    BQUserModel,
    MathQuestionAnswerModel,
    MicrolessonModel,
    StudentMetricsModel,
)

database_for_the_metrics = "supabase-db-rori"
database_for_test="supabase-db-test"

models_for_the_metrics = [
    MessagesModel,
    BQMessageModel,
    BQMessageModelRising,
    BQUserModel,
    MathQuestionAnswerModel,
    MicrolessonModel,
    StudentMetricsModel,
]


class DashboardDBRouter(object):
    # Custom router class for the metrics.
    def db_for_read(self, model, **hints):
        """reading SomeModel from otherdb"""
        if model in models_for_the_metrics:
            return database_for_the_metrics
        return database_for_test

    def db_for_write(self, model, **hints):
        """writing SomeModel to otherdb"""
        if model in models_for_the_metrics:
            return database_for_the_metrics
        return database_for_test
