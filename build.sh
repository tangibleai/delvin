#!/usr/bin/env bash

echo "Install requirements"
pip install -r requirements.txt

echo "Migrating"
python manage.py migrate

echo "Creating a superuser"
python manage.py shell <<EOF
import os
from django.db.utils import IntegrityError
from django.contrib.auth.models import User

username = os.environ["DJANGO_SUPERUSER_USERNAME"]
password = os.environ["DJANGO_SUPERUSER_PASSWORD"]

try:
    User.objects.create_superuser(
	      username = username,
	      password = password,
)
    print(f"User {username} has been created")
    print(f'With a length password: {len(password)}')
except IntegrityError:
    print("User already exists!")
except Exception as err:
    print(err)
exit()
EOF

echo "Collecting static files!"
python manage.py collectstatic --noinput -v 3
