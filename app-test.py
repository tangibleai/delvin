import streamlit as st

from streamlit_frontend.views import (
    analytics,
    question_explorer,
    students,
    conversations,
)

menu_option = st.selectbox(
    "Choose menu option",
    ["Analytics", "Students", "Conversations", "Math Questions"],
    key="app_test_menu",
)

if menu_option == "Analytics":
    analytics.load_page()

if menu_option == "Students":
    students.load_page()

if menu_option == "Math Questions":
    question_explorer.load_page()

if menu_option == "Conversations":
    conversations.load_page()
