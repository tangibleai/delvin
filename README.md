# Delvin

Delvin is a chatbot data platform and a one-stop shop for chatbot analytics built by Tangible AI.


## Installation

After cloning the repository, make sure you install `delvin` in a virtual environment: 

```
$ pip install --upgrade pip virtualenv
$ python -m virtualenv venv
$ source venv/bin/activate 
# for Windows: source venv/Scripts/activate
```

Once the virtual environment is initialized, install the requirements 
```
$ pip install -r requirements.txt 
```
