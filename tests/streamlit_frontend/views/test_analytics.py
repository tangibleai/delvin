from pathlib import Path
import pytest
from datetime import datetime
from streamlit.testing.v1 import AppTest
import time

PROJECT_DIR = Path(__file__).resolve().parent.parent.parent.parent
APP_TEST_FILE = PROJECT_DIR / "app-test.py"
print(str(APP_TEST_FILE))


@pytest.mark.django_db
class TestAnalytics:
    def test_loads(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        assert not at.exception

    def test_loads_with_comparison(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.checkbox(key="analytics_to_compare").set_value(True).run()
        assert not at.exception

    def test_late_start_date(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.date_input(key="analytics_from").set_value(datetime(2024, 12, 1)).run()
        assert at.exception

    # Test tab functionality ==========================
    def test_user_acquisition_load(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[0].multiselect[0].select("1a - Subscribed").run()
        assert not at.exception

    def test_user_progress_load(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[1].selectbox("analytics_metric_to_display").select("num_messages").run()
        assert not at.exception

    def test_user_pathways_load(self):
        # SUGGESTION: key style should be standardized - "an" != "analytics"
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[2].number_input("an_path_lev_prune").increment().run()
        assert not at.exception

    def test_user_responses_load(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[3].text_input("analytics_user_responses_question_text").set_value(
            "2"
        ).run()
        assert not at.exception

    def test_user_responses_bot_question_invalid_input(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[3].text_input("analytics_user_responses_question_text").set_value(
            "jhkasgdhjklaghjlkadsghjlkadg"
        ).run()
        assert not at.exception

    def test_user_responses_popular_questions_loads(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        table_value = at.tabs[3].table[0].value
        assert not table_value.empty

    # Date Testing: Students that joined
    def test_early_date_warning(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.date_input(key="analytics_from").set_value(datetime(2023, 5, 15)).run()
        assert (
            at.warning[0].value
            == "Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing"
        )

    # Date Testing: Compare to date
    def test_early_comp_date_warning(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.checkbox(key="analytics_to_compare").set_value(True).run()
        # Set to one day before May 16th
        at.date_input(key="analytics_from_comp").set_value(datetime(2023, 5, 15)).run()
        assert (
            at.warning[0].value
            == "Warning: Date range includes dates for which math data is not available. Full math data logging started on May 16th, 2023. Math-related charts might be inaccurate or missing"
        )

    def test_user_progress_metrics_to_display_changes(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[1].selectbox("analytics_metric_to_display").select("num_messages").run()

        at.tabs[1].selectbox("analytics_metric_to_display").select(
            "num_math_messages"
        ).run()

        at.tabs[1].selectbox("analytics_metric_to_display").select(
            "num_active_days"
        ).run()

        at.tabs[1].selectbox("analytics_metric_to_display").select(
            "num_active_weeks"
        ).run()

        at.tabs[1].selectbox("analytics_metric_to_display").select(
            "num_lessons_visited"
        ).run()

        at.tabs[1].selectbox("analytics_metric_to_display").select(
            "num_lessons_completed"
        ).run()

        at.tabs[1].selectbox("analytics_metric_to_display").select("num_sessions").run()

        assert not at.exception

    def test_user_progress_user_segment_changes(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.tabs[1].selectbox("analytics_user_progress_user_segment").select("All").run()

        at.tabs[1].selectbox("analytics_user_progress_user_segment").select(
            "2 - Engaged in 1 Math Session"
        ).run()

        at.tabs[1].selectbox("analytics_user_progress_user_segment").select(
            "3 - Returned for 2-5 Math Sessions"
        ).run()

        at.tabs[1].selectbox("analytics_user_progress_user_segment").select(
            "4 - Persisted for >5 Math Sessions"
        ).run()

        assert not at.exception


#     # Known Exceptions ===============================
#     # Analytics > Date 5/17-5/17 (KeyError: 'user_segment)(main date range: 5/16-11/10)

#     # def test_student_join_date_short_span(self):
#     #     # FIXME: KeyError: 'user_segment'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
#     #     at.date_input(key="analytics_from").set_value(datetime(2023, 5, 17)).run()
#     #     at.date_input(key="analytics_to").set_value(datetime(2023, 5, 17)).run()
#     #     assert not at.exception

#     # def test_comp_date_short_span(self):
#     #     # FIXME: KeyError: 'user_segment'
#     #     # FIXME: Inconsistent "to_comp" > "analytics_to_comp"
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
#     #     at.checkbox(key="analytics_to_compare").set_value(True).run()
#     #     at.date_input(key="analytics_from_comp").set_value(datetime(2023, 5, 17)).run()
#     #     at.date_input(key="to_comp").set_value(datetime(2023, 5, 17)).run()
#     #     assert not at.exception

#     # def test_user_pathways_excessive_prune_value(self):
#     #     # FIXME: TypeError: add_subclause()
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
#     #     at.tabs[2].selectbox("an_path_lev_seg").select(
#     #         "3 - Returned for 2-5 Math Sessions"
#     #     ).run()
#     #     at.tabs[2].number_input("an_path_lev_prune").set_value(5000).run()
#     #     at.tabs[2].button("an_path_compute").click().run()

#     #     assert not at.exception

#     # def test_user_pathways_excessive_prune_value(self):
#     #     # FIXME: TypeError: add_subclause()
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
#     #     at.tabs[2].selectbox("an_path_les_seg").select(
#     #         "3 - Returned for 2-5 Math Sessions"
#     #     ).run()
#     #     at.tabs[2].number_input("an_path_les_prune").set_value(5000).run()
#     #     at.tabs[2].button("an_path_les_compute").click().run()
#     #     assert not at.exception
