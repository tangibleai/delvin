from pathlib import Path
import pytest
from datetime import datetime
from streamlit.testing.v1 import AppTest
import time

PROJECT_DIR = Path(__file__).resolve().parent.parent.parent.parent
APP_TEST_FILE = PROJECT_DIR / "app-test.py"
print(str(APP_TEST_FILE))


@pytest.mark.django_db
class TestStudents:
    def test_setting_valid_contact_uuid(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Students").run()
        at.text_input("student_contact_uuid").set_value(
            "356c40b6-ae76-4574-a36c-4d57c05ccbdd"
        ).run()

        """ form_submit_buttons do not have a key
        at.button
        WidgetList(_list=[Button(key='FormSubmitter:Multiple Student Search-Search Multiple Students', label='Search Multiple Students', form_id='Multiple Student Search'), Button(key='FormSubmitter:Single Student Search-Display a Single Student', label='Display a Single Student', form_id='Single Student Search')])
        """
        at.button[1].click().run()
        assert not at.exception

    def test_set_invalid_contact_uuid(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Students").run()
        at.text_input("student_contact_uuid").set_value("5555").run()
        at.button[1].click().run()
        assert not at.exception

    def test_students_submission_displays_metrics(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Students").run()
        at.text_input("student_contact_uuid").set_value(
            "356c40b6-ae76-4574-a36c-4d57c05ccbdd"
        ).run()

        # No metrics before submission
        assert len(at.metric) == 0

        at.button[1].click().run()

        assert len(at.metric) == 4

    def test_advanced_search_with_field_submissions(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()

        at.selectbox("app_test_menu").select("Students").run()

        at.text_input("student_advanced_search_threshold").set_value("5")

        at.selectbox("student_advanced_search_with").select("num_messages").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_with").select(
            "num_lessons_completed"
        ).run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_with").select("num_lessons_visited").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_with").select("num_sessions").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_with").select("num_active_days").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_with").select("num_active_weeks").run()

        at.button[0].click().run()

        assert not at.exception

    def test_advanced_search_condition_field_submissions(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()

        at.selectbox("app_test_menu").select("Students").run()

        at.text_input("student_advanced_search_threshold").set_value("5")

        at.selectbox("student_advanced_search_conditions").select(">").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_conditions").select("<").run()

        at.button[0].click().run()

        at.selectbox("student_advanced_search_conditions").select("=").run()

        at.button[0].click().run()

        assert not at.exception


# Known Exceptions
# def test_incomplete_advanced_search_submission(self):
#     # FIXME: ProgrammingError: syntax error at or near "AND" LINE 2: WHERE num_messages > AND num_lessons_visit... ^
#     # SUGGESTION: Consider adding a default threshold value
#     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
#     at.selectbox("app_test_menu").select("Students").run()

#     at.button[0].click().run()

#     assert not at.exception
