from pathlib import Path
import pytest
from datetime import datetime
from streamlit.testing.v1 import AppTest
import time

PROJECT_DIR = Path(__file__).resolve().parent.parent.parent.parent
APP_TEST_FILE = PROJECT_DIR / "app-test.py"
print(str(APP_TEST_FILE))


@pytest.mark.django_db
class TestConversations:
    def test_conversations_contact_uuid(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
        at.selectbox("app_test_menu").select("Conversations").run()
        at.text_input("conversations_contact_uuid_field").set_value(
            "356c40b6-ae76-4574-a36c-4d57c05ccbdd"
        ).run()
        # at.button[0].click().run()
        assert not at.exception

    def test_blank_look_for_search(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
        at.selectbox("app_test_menu").select("Conversations").run()
        at.button[0].click().run()
        assert not at.exception

    def test_search_conversations_with_criteria(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
        at.selectbox("app_test_menu").select("Conversations").run()

        at.text_input("conversations_look_for_text").set_value("22").run()
        """
        WidgetList(_list=[Button(key='FormSubmitter:Student_id-Display Student', label='Display Student', form_id='Student_id'), Button(key='FormSubmitter:Search Criteria-Search', label='Search', form_id='Search Criteria')])
        """
        at.button[1].click().run()
        assert not at.exception

    def test_conversation_go_to_conversation_result(self):
        # KeyError: 'inserted_at'
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
        at.selectbox("app_test_menu").select("Conversations").run()

        at.text_input("conversations_look_for_text").set_value("22").run()

        at.button[1].click().run()

        at.button[2].click().run()
        assert not at.exception

    # def test_conversation_search_results(self):
    #     # KeyError: 'inserted_at'
    #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
    #     at.selectbox("app_test_menu").select("Conversations").run()
    #
    #     at.text_input("conversations_look_for_text").set_value("22").run()
    #
    #     at.button[1].click().run()
    #     at.button[2].click().run()
    #     at.slider[0].set_value(400).run()
    #     assert not at.exception

    def test_conversation_student_uuid_search_results(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
        at.selectbox("app_test_menu").select("Conversations").run()
        at.button[0].click().run()
        at.slider[0].set_value(100).run()
        assert not at.exception


#     # Known Exceptions
#     # def test_conversations_invalid_contact_uuid(self):
#     #     # FIXME: KeyError: 'inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()
#     #     at.text_input("conversations_contact_uuid_field").set_value("5555").run()
#     #     at.button[0].click().run()
#     #     assert not at.exception

#     # def test_short_date_range(self):
#     #     # FIXME: KeyError: 'message_inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.date_input("conversations_from_date").set_value(datetime(2023, 11, 11)).run()
#     #     at.date_input("conversations_to_date").set_value(datetime(2023, 11, 11)).run()

#     #     at.button[1].click().run()
#     #     assert not at.exception

#     # def test_invalid_look_for_field_value(self):
#     #     # KeyError: 'message_inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.text_input("conversations_look_for_text").set_value("daijadjla").run()

#     #     at.button[1].click().run()
#     #     assert not at.exception

#     # def test_conversation_go_to_conversation_result(self):
#     #     # KeyError: 'inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.text_input("conversations_look_for_text").set_value("234").run()

#     #     at.date_input("conversations_from_date").set_value(datetime(2023, 11, 9)).run()
#     #     at.date_input("conversations_to_date").set_value(datetime(2023, 11, 10)).run()

#     #     at.button[1].click().run()
#     #     """
#     #     WidgetList(_list=[Button(key='FormSubmitter:Student_id-Display Student', label='Display Student', form_id='Student_id'), Button(key='FormSubmitter:Search Criteria-Search', label='Search', form_id='Search Criteria'),
#     #     Button(key='ABEGIzVBeVBUAhDo_66oBX3iQWdtBgVJjMf6convo', label='Go to Conversation'), Button(key='ABEGIzVAGAUxAhCQUqQl3fPc5DXycd2wgb-Hconvo', label='Go to Conversation'), Button(key='ABEGIzVBgDkWAhC7wis3WZ1aXMA_LUWBzwJ_convo', label='Go to Conversation'), Button(key='ABEGIzJJZUUpAhCvV7VrLbLHNkidQROXb2N7convo', label='Go to
#     #     Conversation'), Button(key='ABEGIzJJgjaWAhDiEFAtkjvEiKbxOHCJaJDjconvo', label='Go to Conversation'), Button(key='ABEGIzVBeVBUAhCDEyRBOf6kVsYfz1aA6L4Tconvo', label='Go to Conversation'), Button(key='ABEGIzVAM4eIAhB0euywOjcj7PAN3XZUNTs2convo', label='Go to Conversation'), Button(key='ABEGIzVAhXgVAhCLWTq_H2YQHGoOsU3a0UCXconvo', label='Go to Conversation'), Button(key='ABEGIzVBhAkiAhCuiZb4T_DOoDUYmS5PnKT-convo', label='Go to Conversation'), Button(key='ABEGIzJJcyiZAhCdZUIii2XFQQxRl1CsQzNgconvo', label='Go to Conversation'), Button(key='ABEGIzVBaUgWAhCYMp50QETog3i1y7a5wFTbconvo', label='Go to Conversation')])
#     #     """
#     #     at.button[2].click().run()
#     #     assert not at.exception

#     # def test_invalid_look_for_field_value(self):
#     #     # KeyError: 'message_inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.text_input("conversations_look_for_text").set_value("daijadjla").run()

#     #     at.button[1].click().run()
#     #     assert not at.exception

#     # def test_conversation_go_to_conversation_result_fraction(self):
#     #     # KeyError: 'inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.text_input("conversations_look_for_text").set_value("2/4").run()

#     #     at.button[1].click().run()
#     #     at.button[2].click().run()
#     #     assert not at.exception

#     # def test_conversation_go_to_conversation_result_decimal(self):
#     #     # KeyError: 'inserted_at'
#     #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=60).run()
#     #     at.selectbox("app_test_menu").select("Conversations").run()

#     #     at.text_input("conversations_look_for_text").set_value("2.1").run()

#     #     at.button[1].click().run()
#     #     at.button[2].click().run()
#     #     assert not at.exception
