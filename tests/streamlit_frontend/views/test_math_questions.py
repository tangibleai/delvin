from pathlib import Path
import pytest
from datetime import datetime
from streamlit.testing.v1 import AppTest
import time

PROJECT_DIR = Path(__file__).resolve().parent.parent.parent.parent
APP_TEST_FILE = PROJECT_DIR / "app-test.py"
print(str(APP_TEST_FILE))


@pytest.mark.django_db
class TestMathQuestion:
    def test_loads(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        assert not at.exception

    def test_both_lines_choose_chart_types_most(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Math Questions").run()
        at.selectbox("math_questions_choose_chart_type").select(
            "Popular Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Hints in Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Difficult Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Difficult Questions"
        ).run()
        assert not at.exception

    def test_both_lines_choose_chart_types_least_and_topic_skill(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Math Questions").run()

        at.selectbox("math_questions_chart_labels").select(
            "Lesson + Topic + Skill"
        ).run()

        at.radio("math_questions_most_least").set_value("Least").run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Popular Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Hints in Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Difficult Micro-Lessons"
        ).run()

        at.selectbox("math_questions_choose_chart_type").select(
            "Difficult Questions"
        ).run()
        assert not at.exception

    def test_both_lines_select_level(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Math Questions").run()

        at.selectbox("math_questions_select_level").select("Level 1").run()

        at.selectbox("math_questions_select_level").select("Level 2").run()

        at.selectbox("math_questions_select_level").select("Level 3").run()

        at.selectbox("math_questions_select_level").select("Level 4").run()

        at.selectbox("math_questions_select_level").select("Level 5").run()

        at.selectbox("math_questions_select_level").select("Level 6").run()

        at.selectbox("math_questions_select_level").select("Level 7").run()

        at.selectbox("math_questions_select_level").select("Level 8").run()

        at.selectbox("math_questions_select_level").select("Level 9").run()
        assert not at.exception

    def test_all_question_explorer_settings(self):
        at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
        at.selectbox("app_test_menu").select("Math Questions").run()

        at.selectbox("math_questions_select_level").select("Level 5").run()

        at.selectbox("math_questions_explorer_topic_selection").select("Level 5")

        at.selectbox("math_questions_explorer_skill_selection").select(
            "Data and graphs"
        )

        at.selectbox("math_questions_explorer_lesson_selection").select("G5.S1.1.2.1")

        at.selectbox("math_questions_explorer_question_selection").select("2")

        assert not at.exception

    # Known Exceptions
    # def test_both_lines_short_date_span(self):
    #     # KeyError: 'question_level'
    #     at = AppTest.from_file(str(APP_TEST_FILE), default_timeout=30).run()
    #     at.selectbox("app_test_menu").select("Math Questions").run()

    #     at.date_input("math_question_from_date").set_value(datetime(2023, 11, 11)).run()
    #     at.date_input("math_question_to_date").set_value(datetime(2023, 11, 11)).run()
    #     assert not at.exception

    # NOTE: Least | Difficult Questions | 5 | Lesson + Topic + Skill doesn't seem to have bars on graph

    # NOTE: Most | Popular Micro-lessons | 50 | Lesson name only | Exclude microlessons with <100 answers // gives 2 huge bars

    # NOTE: Unusual behavior: Set Level 9 > Set 2023/09/01 > Set 2023/09/02 - Shows overview, not "Level 9 data" (changing dates resets every field)

    # FIXME: "Students by number of lessons tried" > " at leastthe percent of lessons on axis" // Add a space between "least" and "the"
