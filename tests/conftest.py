import pytest
import os, dotenv
from django.conf import settings

dotenv.load_dotenv()


@pytest.fixture(scope="session")
def django_db_setup():
    settings.DATABASES["default"] = (
        {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": "postgres",
            "HOST": os.environ.get("SUPABASE_DB_RORI"),
            "PASSWORD": os.environ.get("SUPABASE_PASSWORD_RORI"),
            "PORT": 5432,
            "USER": os.environ.get("SUPABASE_USER"),
        },
    )
